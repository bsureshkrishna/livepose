# LivePose release notes


LivePose 1.11.0 (2023-05-12 15:51:46 +0000)
-------------------------

**Bugs fixed**
* Added a second separate stopping flag for no camera frame available sat-mtl/tools/livepose!352 b01f641 2023-04-26
* Fixed sync issue with rosbag chunk writing sat-mtl/tools/livepose!345 1e1a47d 2023-04-19
* Websocket connections reconnect automatically with multiple clients/servers sat-mtl/tools/livepose!340 1c98d76 2023-03-14
* Fix pyrealsense not accepting 320x240 resolution sat-mtl/tools/livepose!343 c37af4f 2023-03-14
* Remove unused variables in context.py sat-mtl/tools/livepose!338 47d8475 2023-03-13
* Fixed default name change from chunk to chunk sat-mtl/tools/livepose!337 5bc6a3e 2023-03-07
* Fix ws connection race condition sat-mtl/tools/livepose!334 97af985 2023-03-07
* Now keeps running with no cameras sat-mtl/tools/livepose!329 0ec8bbd 2023-02-24
* Threshold option in action filter LivePose
* Setting framerate in pyrealsense2
* Profiling by moving it into the context

**Features**
* Added merge rosbag files sat-mtl/tools/livepose!350 a7f4e66 2023-05-23
* Added rosbag camera type sat-mtl/tools/livepose!351 cdfad31 2023-05-04
* Add a parameter to Pose2DToFloor to select which keypoints to follow sat-mtl/tools/livepose!348 b355286 2023-04-19
* Added a threshold parameter to pose2d_to_floor sat-mtl/tools/livepose!347 e895049 2023-04-19
* Improved rosbags file timestamp sat-mtl/tools/livepose!346 e6af241 2023-04-19
* Make detected object drawing optional, dependent on the hide flag sat-mtl/tools/livepose!342 7009082 2023-03-14
* Add async recording to rosbag output sat-mtl/tools/livepose!339 cee8d93 2023-03-13
* Timestamps are now saved in streams and updated when updating frames sat-mtl/tools/livepose!336 7647ede 2023-03-07
* Now saves rosbags in multiple chunks of X seconds sat-mtl/tools/livepose!332 03f03a6 2023-03-07
* Add framerate limiter when no camera is configured sat-mtl/tools/livepose!335 74fe8b1 2023-03-07
* Nodal architecture sat-mtl/tools/livepose!328 bd1b2c8 2023-02-24
* Added rosbag to rawframes tool


LivePose 1.10.2 (2023-02-15 22:01:24 -0500)
-------------------------
**Bug Fixes**
* Fixed Websocket output not closing gracefully sat-mtl/tools/livepose!331 a32995e 2023-02-09
* Fixed Websocket output example configuration sat-mtl/tools/livepose!330 377a21c 2023-02-09
* Fix frame action detection: init and log sat-mtl/tools/livepose!327 faa839b 2023-01-24
* Fix remove cameras when closed, to exit properly at then end of source files sat-mtl/tools/livepose!325 733c0ca 2023-01-23
* Fix action detection filters: check label map size, load config and checkpoint files from mmaction root parameter sat-mtl/tools/livepose!326 f77efda 2023-01-23
* Fix livepose/context.py: log node import error sat-mtl/tools/livepose!323 b86713e 2023-01-19


LivePose 1.10.0 (2023-01-13 15:48:53 +0000)
-------------------------
**Features**
* Added showing input color images if no pose backend color output is available sat-mtl/tools/livepose!318 f87c2ed 2023-01-13
* Added output recording to rosbags sat-mtl/tools/livepose!312 3220cbb 2023-01-09


LivePose 1.9.2 (2022-12-22 15:15:00 +0000)
-------------------------
**Documentation**
* doc: add Citation sat-mtl/tools/livepose!313 34e6fb0 2022-12-08

**Bug Fixes**
* Fixed pose backends color outputs being malformed sat-mtl/tools/livepose!316 f9ccbe4 2022-12-22
* Move the updated ballant example file to the repo itself, from the blog sat-mtl/tools/livepose!315 27f46cb 2022-12-21
* Fix/ballant example sat-mtl/tools/livepose!309 9a30e51 2022-11-28


LivePose 1.9.0 (2022-11-22 21:17:59 +0000)
-------------------------
**Features**
* Feature: improve frame and pose action detection filter (labels with scores, re-align with mmaction demos) sat-mtl/tools/livepose!262 798be86 2022-11-22
* Feature: improve frame action detection for better performance (time and action detection accuracy) sat-mtl/tools/livepose!260 1f45113 2022-11-22


LivePose 1.8.4 (2022-11-18 23:43:46 +0000)
-------------------------
**Bug Fixes**
* Fix livepose/pose_backends/mmpose.py: populate keypoints_definitions sat-mtl/tools/livepose!311 a958610 2022-11-18
* Fix livepose/pose_backends/mmpose.py: fix super().parse_parameters(*args,**kwargs) sat-mtl/tools/livepose!310 9d47fdc 2022-11-17


LivePose 1.8.2 (2022-11-09 15:29:54 +0000)
-------------------------
**Bug Fixes**
* Fix: fix default arguments flagged by mypy 0.990 sat-mtl/tools/livepose!308 25b122f 2022-11-09


LivePose 1.8.0 (2022-11-07 13:08:51 +0000)
-------------------------
**Features**
* Feature: switch to ubuntu 22.04 jammy sat-mtl/tools/livepose!307 e22e923 2022-11-07
* Feature: add mediapipe pose backend sat-mtl/tools/livepose!305 ec22594 2022-11-03
* Feature: update doc and setup with mmpose package and models sat-mtl/tools/livepose!301 0d64551 2022-11-03
* Feature: require lap and install python3-lap sat-mtl/tools/livepose!306 f50bafa 2022-11-03
* Feature: list params sat-mtl/tools/livepose!270 f3fbb72 2022-10-26


LivePose 1.7.2 (2022-10-06 19:03:03 -0400)
-------------------------
**Bug Fixes**
* Fix: RuntimeWarning: invalid value encountered in true_divide at h = x[2] / w in livepose/extlib/sort.py sat-mtl/tools/livepose!304 d66f94d 2022-10-06
* Fix: KeyError exception in dimmaps pose2d_to_floor sat-mtl/tools/livepose!303 18b90aa 2022-10-06
* Fix: liblo output crashes with mmpose backend sat-mtl/tools/livepose!300 cb8e023 2022-09-21


LivePose 1.7.0 (2022-09-21 18:43:31 +0000)
-------------------------
**Documentation**
* Update Jetson installation setup and docs sat-mtl/tools/livepose!193 b535665 2022-09-21


LivePose 1.6.2 (2022-09-15 21:00:12 +0000)
-------------------------
**Documentation**
* Doc CI optimize: remove website generation, remove metalab PPA, add datasets MPAs, reorder dependencies lists sat-mtl/tools/livepose!299 653ebde 2022-09-15

**Bug Fixes**
* Fix: setup: print git submodule error sat-mtl/tools/livepose!298 814dfbb 2022-09-15


LivePose 1.6.0 (2022-09-12 19:54:12 +0000)
-------------------------
**Features**
* Added the possibility to have multiple pose backends sat-mtl/tools/livepose!296 fcc035d 2022-09-12
* Added a pose completeness option to the skeletons filter sat-mtl/tools/livepose!290 4cd1c35 2022-09-12
* Added camera calibration to realsense_2d_to_3d dim map sat-mtl/tools/livepose!289 447d9f4 2022-09-12
* Added an option for skeleton filter to output 2D and 3D poses sat-mtl/tools/livepose!287 e445fca 2022-09-12
* Added client connection to Websocket output sat-mtl/tools/livepose!286 b1e4cc2 2022-09-12

**Bug Fixes**
* Fixed realsense_2d_to_3d sending zeroed coords in some cases sat-mtl/tools/livepose!285 d8bdf79 2022-09-12


LivePose 1.5.2 (2022-09-07 20:34:50 +0000)
-------------------------
**Bug Fixes**
* Fix: configs for packaging sat-mtl/tools/livepose!294 83f2026 2022-09-07
* Fix: bionic dependencies: dataclasses instead of keylime and pyliblo instead of pyliblo3 sat-mtl/tools/livepose!293 9fce991 2022-09-07
* Fix: sort.py: optional display dependencies sat-mtl/tools/livepose!292 8be9dac 2022-09-07
* Fix: add missing setup.py packages sat-mtl/tools/livepose!291 d7a9abc 2022-09-07


LivePose 1.5.0 (2022-09-06 20:44:49 +0000)
-------------------------
**Features**
* Feature: mmpose hands sat-mtl/tools/livepose!284 b1c31d8 2022-09-06


LivePose 1.4.2 (2022-09-06 18:29:49 +0000)
-------------------------
**Bug Fixes**
* Fix: migrate inside sat-mtl sat-mtl/tools/livepose!288 5380fa2 2022-09-06


LivePose 1.4.0 (2022-08-31 16:59:26 -0400)
-------------------------
**Features**
* Update mmcv_full==1.6.1 mmdet==2.25.1 mmpose==0.28.1 sat-mtl/tools/livepose!283 746d0d3 2022-08-30
* Cameras are now shown in their own window sat-mtl/tools/livepose!282 abc1394 2022-08-29


LivePose 1.3.2 (2022-08-29 20:43:12 +0000)
-------------------------
**Bug Fixes**
* Fixed crash when reprojected position is outside the frame in realsense_2d_to_3d sat-mtl/tools/livepose!281 5b48469 2022-08-29
* Fixed bounding boxes tracking with multiple cameras sat-mtl/tools/livepose!280 3cfab95 2022-08-26
* Fixed Pose2DToFloor output, which still matched previous OSC-centric layout sat-mtl/tools/livepose!278 c2fd6b4 2022-08-26


LivePose 1.3.0 (2022-08-26 17:28:14 +0000)
-------------------------
**Features**
* Added Realsense2DTo3D dimension mapper sat-mtl/tools/livepose!277 9dc0803 2022-08-26
* Feature: trt local optimized model path sat-mtl/tools/livepose!267 21556e9 2022-08-24

**Documentation**
* Update: doc: instructions for nvidia drivers sat-mtl/tools/livepose!279 b880e77 2022-08-26


LivePose 1.2.2 (2022-08-24 14:59:27 +0000)
-------------------------
**Bug Fixes**
* Fix: livepose/extlib/sort.py: RuntimeWarning: invalid value encountered in double_scalars at  r = w / float(h) sat-mtl/tools/livepose!276 652da02 2022-08-24
* Fix: doc: replace PPA by MPA and add missing dependencies sat-mtl/tools/livepose!275 5ea841d 2022-08-24


LivePose 1.2.0 (2022-08-23 21:44:16 +0000)
-------------------------
**Features**
* Refactored Filter results and Output to be less OSC-centric sat-mtl/tools/livepose!274 19e230e 2022-08-23
* Added PosesForCameras, Pose and Keypoint classes to improve code readability sat-mtl/tools/livepose!272 ff4e7a9 2022-08-23


LivePose 1.1.2 (2022-08-16 22:09:14 +0000)
-------------------------
**Bug Fixes**
* Updated Metalab root URL sat-mtl/tools/livepose!273 578dbbc 2022-08-16
* Fixed flip and rotate of input video channels !271 24b1cec 2022-08-04


LivePose 1.1.0 (2022-07-29 19:34:56 +0000)
-------------------------
**Features**
* Use pyrealsense2 from mpa !266 fcf4e09 2022-07-29
* Added depth clipping to pyrs2 head tracker !269 9775ca5 2022-07-29
* Added pose tracking to all pose backends !268 ed419d8 2022-07-25
* Add config param rotate camera !264 193d78d 2022-07-07


LivePose 1.0.4 (2022-06-22 22:49:24 +0000)
-------------------------
**Bug Fixes**
* Fix: flip_camera !261 b81a99b 2022-06-22
* Updated eye follower to follow the whole head !259 64bc679 2022-06-22
* Only load components listed in config !263 4e3c911 2022-06-21
* Pass depth in the position filter !257 b3641e5 2022-06-03
* Fix frame/pose action detection filters !256 a149e34 2022-06-03


LivePose 1.0.2 (2022-06-02 15:13:25 +0000)
-------------------------
**Bug Fixes**
* Fixed playing rosbag files using pyrealsense2 camera when no depth in stream... !258 7d361d7 2022-06-02


LivePose 1.0.0 (2022-05-24 14:45:31 +0000)
-------------------------
**Features**
* Added back framerate display in the console !250 f41625e 2022-05-24
* Added a new dimension mapping stage to the pipeline !244 6ddf0ce 2022-05-19
* Added color to depth conversion to the eye follower instead of inside the pyrealsense camera !252 457171c 2022-05-13
* Added a pyrealsense2-based eye follower filter !251 07dafed 2022-05-11
* Added Flow and Stream to handle dataflow in the pipeline !239 f2ea265 2022-05-05

**Improvements**
* Renamed DLBackend to PoseBackend, and updated other dl backend occurrences !243 d68c33d 2022-05-04
* Removed unnecessary mmaction2 related files !248 7c2d384 2022-05-04
* Fixed code style with autopep8 !247 4e327f6 2022-05-04

**Bug Fixes**
* Fixed pyrealsense rosbag camera index test !254 5aaaeff 2022-05-18
* Fixed trt_pose not outputting numpy arrays !253 c630538 2022-05-13
* Fixed pyrealsense2 support, removed Camera.frames which brought confusion with the Flow !249 a32db79 2022-05-11

**Documentation**
* Added documentation to run LivePose using Docker image !255 617ec8e 2022-05-19


LivePose 0.13.0 (2022-05-04 17:16:38 +0000)
-------------------------
**Features**
* Added a pointing filter !238 2bfeb2d 2022-05-04
* Added mmaction-based filters !230 c50e7f7 2022-05-04

**Bug Fixes**
* Removed unnecessary mmaction2 related files !248 7c2d384 2022-05-04
* mypy: check and fix setup.py !246 6de1a6e 2022-05-03


LivePose 0.12.4 (2022-05-03 18:42:11 +0000)
-------------------------
**Bug Fixes**
* Fix mypy: check and fix setup.py !246 6de1a6e


LivePose 0.12.2 (2022-05-03 15:18:41 +0000)
-------------------------
**Bug Fixes**
* Fix: doc: contributing: linting !245 0085d96
* Fix: .gitlab ci.yml+doc: fix apt key for NVIDIA CUDA repo !242 fc5b500


LivePose 0.12.0 (2022-04-14 14:19:29 +0000)
-------------------------
**Features**:
* Removed the OpenPose (original and reimplemented with OpenCV) backends !237 1933821 
* Added pose drop based on overall confidence in trtpose !236 2aa4de3
* Added position merging based on distance to position2D filter !234 7449aa4


LivePose 0.11.2 (2022-04-01 20:30:26 +0000)
-------------------------
**Bug Fixes**:
* Fixed framerate not being computed (lost in refactoring) !231 2534186
* Fixed arguments handling !232 4c7af8a


LivePose 0.11.0 (2022-03-23 08:31:04 -0400)
-------------------------
**Features**:
* Add release script forked from Splash !227 faaf66f


LivePose 0.10.0 (2022-03-18 01:07:50 +0000)
-------------------------
**Features**:
* Added LivePoseContext and converted LivePose to being a full-fledged module !228 a0081b2


LivePose 0.9.0 (2022-03-09 00:47:56 +0000)
-------------------------
**Features**:
* Add mmaction2 filter and json output !216 46016f4


LivePose 0.8.2 (2022-02-28 21:01:18 +0000)
-------------------------
**Documentation**:
* doc/logging: better inform on setting up optional extras !219 b1731d1

**Features**:
* Added the 'id' configuration flag to select the Realsense camera to read from !222 be49332

**Bug Fixes**:
* Rename mpa path: forks to distributions !225 1c3aa36
* setup.py: force mmpose==0.22.0 !224 197f111
* setup.py: force xtcocotools==1.10 !223 ae1ef24


LivePose 0.8.0 (2022-02-25 16:34:03 +0000)
-------------------------
**Features**:
* Added a planar position filter, which works on arbitrary orientated planes !220 8eb1788


LivePose 0.7.2 (2022-02-17 13:47:03 +0000)
-------------------------
**Documentation**:
* Example/ballant translate !217 42742d5
* Contributing: update setup.py for each new optional filter/component !221 5c3fd26

**Features**:
* Optional pyrealsense2 !218 ecda8e6


-------------------------
**Features**:
* Add mmpose backend !201 5ff4d03


LivePose 0.6.6 (2022-02-04 18:56:59 +0000)
-------------------------
**Bug Fixes**:
* Reorganize results file !215 3cc06bd
* Add option to run mot challenge on different backends !214 cd5112a
* Delete zero-width bounding boxes in OpenPose !213 9b1221c
* Fixed osc-web URL not being mentioned in .gitmodules !212 3bcd5ca
* Fix bug in sort object tracking tracker deletion !211 2791e62
* Add more mot20 experiment results !209 3559115
* .gitignore artifacts from coverage and profiling !210 97f27b2
* Optional keypoints dicts passed from dl_backends to filters and outputs !208 978343b

LivePose 0.6.4 (2022-01-19 11:36:11 -0500)
-------------------------
**Features**:
* Add test results for OpenPose max_age MOT20 evaluations !207 f2676ec


LivePose 0.6.2 (2022-01-14 18:07:08 +0000)
-------------------------
**Features**:
* Add time profiling to mot20 script !206 634268d
* catch/except if reboot needed after cuda drivers update !205 bbe85bf


LivePose 0.6.0 (2022-01-13 14:16:07 +0000)
-------------------------
**Features**:
* Ballant: a LivePose example !173 a1775c9


LivePose 0.5.2 (2022-01-06 19:21:58 +0000)
-------------------------
**Bug Fixes**:
* fix mypy: support numpy 1.20 !202 3938cb6
* fix .gitlab-ci.yml: faster with pip3 install -e . !203 344b292


LivePose 0.5.0 (2022-01-04 17:05:58 +0000)
-------------------------
**Features**:
* Implement trt backend !190 ff1066a


LivePose 0.4.2 (2022-01-03 22:28:18 +0000)
-------------------------
**Bug Fixes**:
* setup.py: make void dl backend optional !200 75a9d2d 2022-01-03

**Features**:
* Make dependency-specific implementations (filters/outputs) optional at setup time or runtime !199 1ce975b 2022-01-03


LivePose 0.4.0 (2022-01-03 20:31:35 +0000)
-------------------------
**Features**:
* Read rosbag files with pyrealsense2 api !198 4f62a59


LivePose 0.3.2 (2021-12-22 23:25:07 +0000)
-------------------------
**Bug Fixes**:
* Fix OpenCV DNN CUDA backend check !197 84973c9
* Fix camera parameters init to re-enable single image as input !196 b787225


LivePose 0.3.0 (2021-12-22 17:44:46 +0000)
-------------------------
**Features**:
* Add scripts to evaluate openpose models on coco validation set !139 a080ff5


LivePose 0.2.2 (2021-12-20 19:06:07 +0000)
-------------------------
**Bug Fixes**:
* Set v4l2 exposure_auto_priority to 0 if control available !194 b4856b8

**Documentation**:
* Improved Splash addon documentation (and script, a bit) !195 8be902a


LivePose 0.2.0 (2021-12-10 17:36:08 +0000)
-------------------------
**Features**:
* Compute joints 3D position from 2D pose and depth map with pyrealsense2 !158 ea29ad9


LivePose 0.1.0 (2022-03-23 07:02:55 -0400)
-------------------------
**Features**:
* Implement libmapper output filter !189 06b5123


LivePose 0.0.16 (2021-12-07 18:04:57 +0000)
-------------------------
**Bug Fixes**:
* Improve FPS by setting v4l2 exposure_auto_priority to 0 !191 91e6adf


LivePose 0.0.14 (2021-12-07)
----------------------------
**Features**:
* Face and Hand detection (only works for one hand/face in frame at a time).
* Profiling tools added (with documentation in
[`Contributing.md`](Contributing.md))
* Pose tracking between frames now supported and included in filter outputs (only
for OpenPose backend).
* Improved logging messages when CUDA and/or OpenCV are not installed correctly.

**Bug Fixes**:
* Running with `--hidden` and `--wait` options together no longer causes a
crash. The user can now manually proceed through frames even in hidden mode.

**Documentation**:
* Simplified base installation doc to include instructions ONLY for default
setup (Ubuntu 20.04 with GPU support). All other installation instructions
moved to separate files
* Updated Authors list.


LivePose 0.0.12 (2021-11-02)
----------------------------
**Deprecated**:
* Jump filter has been removed, as it was untested and unused

LivePose 0.0.10 (2021-10-19)
----------------------------
**Bug Fixes**:
* Fixed keypoint mapping when using OpenPose with the OpenCV DNN implementation.
The OpenPose wrapper only uses the BODY_25 keypoint model, while the OpenCV
implementation only uses the COCO 18 keypoint model. When using the OpenCV
implementation, the COCO keypoints are now mapped to their BODY_25 counterparts
and output with the correct names and indices.

LivePose 0.0.8 (2021-10-19)
---------------------------
**Features**:
* OpenPose can now be run with the OpenCV DNN Module, in place of the official
OpenPose wrapper library.
* Improved logging messages for Splash addon.

**Documentation**:
* Clearer documentation for setting Websocket host.

LivePose 0.0.6 (2021-10-15)
---------------------------
**Bug Fixes**:
* Fixed OpenPose crashing whenever no poses were in frame.

LivePose 0.0.4 (2021-10-12)
---------------------------
**Documentation**:
* Added example OSC server script
[tools/osc_server.py](tools/osc_server.py) for printing filter results.


LivePose 0.0.2 (2021-10-01)
---------------------------
* First Pre-Alpha Release

**Features**:
* OpenPose and PoseNet pose estimation backends
* Pre-Packaged dependencies in PPA for installation
* OSC and Websocket outputs
* Filters for outputting differnt kinds of data (keypoint lists, "arm-up" detection, etc.)
