// poses possibles dans le jeu
var possible_poses = ['lever le bras gauche !',
             'lever le bras droit !',
             'baisser les deux bras.'];

// pose à effectuer
var required_pose = "";

// variables d'état permettant de garder en mémoire 
// si l'un ou l'autre des bras a été levé récemment.
var armleft_up = 0;
var armright_up = 0;

// permet de choisir au hasard une pose parmis les poses possibles.
function getPose(pose){
    return possible_poses[getRandomInt(possible_poses.length)];
};

// génère un chiffre aléatoire valant au plus `max`. 
// pour plus d'information : 
// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

// message à afficher lorsque la pose a été effectuée avec succès
function sayCongrats(){
    document.getElementById("congrats").innerHTML = "Bravo !";
}

// message à afficher lorsqu'on doit ré-essayer de nouveau.
function sayTryAgain(){
    document.getElementById("congrats").innerHTML = "Vous avez levé au moins un bras... Essayez à nouveau.";
}

// remise à zéro du jeu lorsqu'on demande une nouvelle pose.
function changePose(){
    document.getElementById("congrats").innerHTML = "Essayez à nouveau.";
    required_pose = getPose(possible_poses);
    armleft_up = 0;
    armright_up = 0;
    document.getElementById("action").innerHTML = required_pose;
}

// on appelle la fonction qui génère de nouvelles poses à chaque 3 secondes.
setInterval(changePose,3000);

// adresse socket pour communiquer avec LivePose
var socket = io('http://127.0.0.1:8081');

// informations de connexion pour communiquer avec LivePose
socket.on('connect', function() {
        socket.emit('config',
              {
                    server: {
                    port: 9000,
                    host: 'localhost'
                },
                    client: {
                    port: 3334,
                    host: '127.0.0.1'
                }
            });
});

// la boucle de jeu.
// pour chaque message reçu lors de la connexion à LivePose, on analyse le 
// contenu et on agit en conséquence.
socket.on('message', function(obj) {
    
    // si le message concerne la position d'un bras...
    if (obj[0].includes("armup")) {

        // ... alors on garde en mémoire de quel bras il s'agit...
        var current_arm = obj[0];
        // ... et quel est le statut (levé ou abaissé) du bras.
        var current_status = obj[1];
        
        if (current_status == 1){
        
            if (current_arm.includes('left')){
        
                armleft_up = 1;
        
            } else if (current_arm.includes('right')){
        
                armright_up = 1;
        
            }
        }

        // Action(s) à prendre selon quelle pose est réalisée par la personne jouant.
        if (required_pose == possible_poses[0]){
            
            // ici, la pose demandée par le jeu est de lever le bras gauche.
            // si le bras gauche est levé et que le bras droit est abaissé, 
            // alors on affiche un message de succès
            if ( armleft_up == 1 && armright_up == 0 ){

                sayCongrats();
            }
    
        } else if (required_pose == possible_poses[1]){
            
            // de la même façon, si la pose demandée est de lever le bras droit
            // et que le bras droit est levé alors que le bras gauche est baissé,
            // alors on affiche un message de succès
            if ( armright_up == 1 && armleft_up == 0){

                sayCongrats();
            }
    
        } else if (required_pose == possible_poses[2]){
            
            // et enfin, si on doit baisser les bras, alors on affiche un 
            // message d'erreur dès qu'un des deux bras est levé, sinon on 
            // affiche un message de succès.
            if ( armright_up == 1 || armleft_up == 1){
            
                sayTryAgain();

            } else {
            
                sayCongrats();
            }
        }
    }
});


