import logging
import os
import sys
from enum import IntEnum

from typing import Any, Dict, List, Optional, Tuple

import cv2
import numpy as np

from livepose.dataflow import Channel, Flow, Stream
from livepose.pose_backend import PosesForCamera, Pose, Keypoint, PoseBackend, register_pose_backend
from livepose.node import ranged_type

logger = logging.getLogger(__name__)

try:
    import mediapipe as mp
    mp_drawing = mp.solutions.drawing_utils
    mp_faces = mp.solutions.face_detection
    mp_face_mesh = mp.solutions.face_mesh
    mp_hands = mp.solutions.hands
    mp_holistic = mp.solutions.holistic
    mp_pose = mp.solutions.pose
    has_mediapipe = True
except ImportError:
    has_mediapipe = False


@register_pose_backend("mediapipe")
class MediaPipeBackend(PoseBackend):
    """
    Void backend, mainly for testing performance of other LivePose components
    """

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()
        self._parser.add_argument('--detect-faces', type=bool, default=False,
                                  help="Whether to detect faces or not.")
        self._parser.add_argument('--detect-face-meshs', type=bool, default=False,
                                  help="Whether to detect face meshs or not.")
        self._parser.add_argument('--detect-hands', type=bool, default=True,
                                  help="Whether to detect hands or not.")
        self._parser.add_argument('--detect-poses', type=bool, default=False,
                                  help="Whether to detect poses or not.")
        self._parser.add_argument('--detect-holistic', type=bool, default=False,
                                  help="Whether to use holistic detection (face meshs and hands and poses) or not.")
        self._parser.add_argument('--use-gpu', type=bool, default=True,
                                  help="Whether to use GPU or not.")
        self._parser.add_argument('--min-detection-confidence', type=ranged_type(float, 0.0, 1.0), default=0.5,
                                  help="Minimum detection confidence.")

    def init(self) -> None:
        if not has_mediapipe:
            logger.error("mediapipe library could not be found. Have you installed it?")

        if self._args.use_gpu:
            logger.info("checking GPUs")
            cv2_cuda_enabled_device_count = 0
            try:
                cv2_cuda_enabled_device_count = cv2.cuda.getCudaEnabledDeviceCount()
            except cv2.error as e:
                logger.error(f"{e.msg}")
                if e.code == -217:
                    # -217: Gpu API call
                    if e.err.find("forward compatibility was attempted on non supported HW") != -1 or e.err.find("system has unsupported display driver / cuda driver combination") != -1:
                        logger.error(
                            "This error may be due to a recent upgrade of Nvidia drivers, if so you may need to reboot your computer.")
                    sys.exit(1)
            if cv2_cuda_enabled_device_count == 0:
                logger.warning("no CUDA devices detected")
            else:
                logger.info("enabling NVIDIA dGPU")
                os.environ['__NV_PRIME_RENDER_OFFLOAD'] = str(1)
                os.environ['__GLX_VENDOR_LIBRARY_NAME'] = 'nvidia'
        if hasattr(mp.python._framework_bindings, 'gpu') and hasattr(mp.python._framework_bindings.gpu, 'use_gpu'):
            mp.python._framework_bindings.gpu.use_gpu(self._args.use_gpu)

        if self._args.detect_faces:
            self._face_detection = mp_faces.FaceDetection(
                min_detection_confidence=self._args.min_detection_confidence)
        if self._args.detect_face_meshs:
            self._face_mesh = mp_face_mesh.FaceMesh(
                min_detection_confidence=self._args.min_detection_confidence)
        if self._args.detect_hands:
            self._hands = mp_hands.Hands(
                min_detection_confidence=self._args.min_detection_confidence,
                min_tracking_confidence=0.5)
        if self._args.detect_holistic:
            self._holistic = mp_holistic.Holistic(
                smooth_landmarks=True)
        if self._args.detect_poses:
            self._pose = mp_pose.Pose(
                smooth_landmarks=True)

    def start(self) -> bool:
        """Start the void deep learning model.
        :return: bool - success of start. Returns False if there is no valid
        image generator, model path, etc.
        """
        return True

    def landmarks_to_keypoints(self, id: int, landmarks, definitions: IntEnum, score: float) -> Pose:
        keypoints: Dict[str, Keypoint] = dict()
        keypoints_definitions: Dict[str, int] = dict()
        for j, landmark in enumerate(landmarks):
            c = score
            x = float(landmark.x)
            y = float(landmark.y)
            part = definitions(j).name  # type: ignore
            keypoints_definitions[part] = j
            keypoints[part] = Keypoint(
                confidence=c,
                part=part,
                position=[x, y]
            )
        pose_confidence = score
        pose: Pose = Pose(
            confidence=pose_confidence,
            keypoints=keypoints,
            id=id,
            keypoints_definitions=keypoints_definitions
        )
        return pose

    def step_pose_detection(self, flow: Flow, now: float, dt: float) -> bool:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)

        if len(input_streams) == 0:
            return False

        tracked_images = []
        keypoints_by_cam: List[PosesForCamera] = []

        for stream in input_streams.values():
            if Channel.Type.COLOR not in stream.channel_types:
                return False

            image: np.array = stream.get_channels_by_type(Channel.Type.COLOR)[0].data

            poses: List[Pose] = []

            if self._args.detect_faces:
                results = self._face_detection.process(image)
                if results.detections:
                    for i, detection in enumerate(results.detections):
                        mp_drawing.draw_detection(image, detection)
                        poses.append(self.landmarks_to_keypoints(
                            id=detection.detection_id,
                            landmarks=detection.location_data.relative_keypoints,
                            definitions=mp_faces.FaceKeyPoint,
                            score=detection.score[0]
                        ))
            if self._args.detect_face_meshs:
                results = self._face_mesh.process(image)
                if results.multi_face_landmarks:
                    for i, landmarks in enumerate(results.multi_face_landmarks):
                        mp_drawing.draw_landmarks(
                            image, landmarks, mp_face_mesh.FACEMESH_TESSELATION)
                        poses.append(self.landmarks_to_keypoints(
                            id=i,
                            landmarks=landmarks.landmark,
                            definitions=IntEnum("FaceMeshEnum", ['FACEMESH_{}'.format(i) for i in range(
                                mp.solutions.face_mesh.FACEMESH_NUM_LANDMARKS)], start=0),
                            score=self._args.min_detection_confidence
                        ))
            if self._args.detect_hands:
                results = self._hands.process(image)
                if results.multi_hand_landmarks:
                    # Note: multi_hand_landmarks are normalized to `[0.0, 1.0]`
                    # multi_hand_world_landmarks are real-world 3D coordinates in meters
                    for i, landmarks in enumerate(results.multi_hand_landmarks):
                        mp_drawing.draw_landmarks(
                            image, landmarks, mp_hands.HAND_CONNECTIONS)
                        poses.append(self.landmarks_to_keypoints(
                            id=i,
                            landmarks=landmarks.landmark,
                            definitions=mp_hands.HandLandmark,
                            score=self._args.min_detection_confidence
                        ))
            if self._args.detect_holistic:
                results = self._holistic.process(image)
                # print(dir(results))
                if results.face_landmarks:
                    mp_drawing.draw_landmarks(
                        image, results.face_landmarks, mp_face_mesh.FACEMESH_TESSELATION)
                    poses.append(self.landmarks_to_keypoints(
                        id=0,  # holistic supports only one person
                        landmarks=results.face_landmarks.landmark,
                        definitions=IntEnum("FaceMeshEnum", ['FACEMESH_{}'.format(i) for i in range(
                            mp.solutions.face_mesh.FACEMESH_NUM_LANDMARKS)], start=0),
                        score=self._args.min_detection_confidence
                    ))
                if results.pose_landmarks:
                    mp_drawing.draw_landmarks(
                        image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS)
                    poses.append(self.landmarks_to_keypoints(
                        id=0,  # holistic supports only one person
                        landmarks=results.pose_landmarks.landmark,
                        definitions=mp_pose.PoseLandmark,
                        score=self._args.min_detection_confidence
                    ))
                if results.left_hand_landmarks:
                    mp_drawing.draw_landmarks(
                        image, results.left_hand_landmarks, mp_hands.HAND_CONNECTIONS)
                    poses.append(self.landmarks_to_keypoints(
                        id=0,  # holistic supports only one person and one left hand
                        landmarks=results.left_hand_landmarks.landmark,
                        definitions=mp_hands.HandLandmark,
                        score=self._args.min_detection_confidence
                    ))
                if results.right_hand_landmarks:
                    mp_drawing.draw_landmarks(
                        image, results.right_hand_landmarks, mp_hands.HAND_CONNECTIONS)
                    poses.append(self.landmarks_to_keypoints(
                        id=1,  # holistic supports only one person and one right hand
                        landmarks=results.right_hand_landmarks.landmark,
                        definitions=mp_hands.HandLandmark,
                        score=self._args.min_detection_confidence
                    ))
            if self._args.detect_poses:
                results = self._pose.process(image)
                if results.pose_landmarks:
                    mp_drawing.draw_landmarks(
                        image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS)
                    poses.append(self.landmarks_to_keypoints(
                        id=0,  # holistic supports only one person
                        landmarks=results.pose_landmarks.landmark,
                        definitions=mp_pose.PoseLandmark,
                        score=self._args.min_detection_confidence
                    ))

            keypoints_by_cam.append(PosesForCamera(
                poses=poses
            ))

            tracked_images.append(image)

        self._poses_for_cameras = keypoints_by_cam

        self._tracked_images = tracked_images

        return True

    def get_output(self) -> List[Channel]:
        channels: List[Channel] = [
            Channel(
                type=Channel.Type.POSE_2D,
                name="keypoints",
                data=self._poses_for_cameras,
                metadata={}
            )
        ]

        if self._tracked_images:
            for index, tracked_image in enumerate(self._tracked_images):
                channels.append(Channel(
                    type=Channel.Type.COLOR,
                    name=f"pose_image_{index}",
                    data=tracked_image,
                    metadata={
                        "resolution": [tracked_image.shape[1], tracked_image.shape[0]]
                    }
                ))

        return channels

    def is_gpu_acceleration_active(self) -> bool:
        """Check if GPU acceleration is active."""
        if not hasattr(mp.python._framework_bindings, 'gpu'):
            return False
        if not hasattr(mp.python._framework_bindings.gpu, 'has_gpu'):
            return False
        return mp.python._framework_bindings.gpu.has_gpu()

    def is_gpu_acceleration_needed(self) -> bool:
        """Check if GPU acceleration is needed."""
        return self._args.use_gpu
