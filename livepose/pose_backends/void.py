import logging
import os
import sys

from typing import Any, Dict, List, Optional, Tuple

import cv2
import numpy as np

from livepose.dataflow import Channel, Flow, Stream
from livepose.pose_backend import PoseBackend, register_pose_backend

logger = logging.getLogger(__name__)


@register_pose_backend("void")
class VoidBackend(PoseBackend):
    """
    Void backend, mainly for testing performance of other LivePose components
    """

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

    def add_parameters(self) -> None:
        # void processes no poses so no parameters are needed even from base class
        # super().add_parameters()
        pass

    def init(self) -> None:
        return

    def start(self) -> bool:
        """Start the void deep learning model.
        :return: bool - success of start. Returns False if there is no valid
        image generator, model path, etc.
        """
        return True

    def step_pose_detection(self, flow: Flow, now: float, dt: float) -> bool:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)

        if len(input_streams) == 0:
            return False

        tracked_images = []

        for stream in input_streams.values():
            if Channel.Type.COLOR not in stream.channel_types:
                return False

            image: np.array = stream.get_channels_by_type(Channel.Type.COLOR)[0].data

            tracked_images.append(image)
        self._tracked_images = tracked_images

        return True

    def get_output(self) -> List[Channel]:
        channels: List[Channel] = []
        if self._tracked_images:
            for index, tracked_image in enumerate(self._tracked_images):
                channels.append(Channel(
                    type=Channel.Type.COLOR,
                    name=f"pose_image_{index}",
                    data=tracked_image,
                    metadata={
                        "resolution": [tracked_image.shape[1], tracked_image.shape[0]]
                    }
                ))

        return channels

    def is_gpu_acceleration_active(self) -> bool:
        """Check if GPU acceleration is active."""
        return False

    def is_gpu_acceleration_needed(self) -> bool:
        """Check if GPU acceleration is needed."""
        return False
