import asyncio
import json
import logging
import os
import ssl
import threading
import time
from typing import Any, Dict, List, Optional

# NOTE: Ignore typing for these since mypy does not recognize the aliases
# used by websockets for imports.
# See websockets docs for details on their API / module structure.
import websockets as ws
from websockets import serve  # type: ignore
from websockets.exceptions import ConnectionClosed
from websockets.server import WebSocketServerProtocol  # type: ignore

from livepose.input import register_input, Input

logger = logging.getLogger(__name__)


def create_ssl_context(cert_path: str = "",
                       key_path: str = "") -> Optional[ssl.SSLContext]:
    """ Creates new SSL context.
    Loads a private key and its corresponding certificate.
    :param cert_path: str - Path to the ssl certificate.
    Path should be either absolute or relative to the working directory.
    :param key_path: str - Path to the ssl certificate.
    Path should be either absolute or relative to the working directory.
    directory, to the ssl key.
    :return: ssl.SSLContext - a new SSL context.
    """
    if not cert_path or not key_path:
        return None  # We're not using SSL. That's fine.
    root_path = os.path.realpath('.')
    if os.path.isabs(cert_path):
        cert_abs_path = cert_path
    else:
        cert_abs_path = os.path.join(root_path, cert_path)
    if os.path.isabs(key_path):
        key_abs_path = key_path
    else:
        key_abs_path = os.path.join(root_path, key_path)
    if not os.path.exists(cert_abs_path) or not os.path.exists(key_abs_path):
        raise FileNotFoundError("SSL key or certificate not found. "
                                "Paths should be either absolute or relative to the working folder.")
    ssl_ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    ssl_ctx.load_cert_chain(cert_abs_path, key_abs_path)
    return ssl_ctx


@register_input("websocket")
class WebsocketInput(Input):
    """
    Class which send out filter results via websocket.
    :param host: Host address on which the server will accept incoming connections
    :param port: Server port
    """

    def __init__(self, **kwargs: Any):
        super(WebsocketInput, self).__init__("websocket", **kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()
        self._parser.add_argument("--server", type=Dict[str, Any], default={},  # type: ignore
                                  help="Server config: host, port, ssl")

    def init(self) -> None:
        # Results stored for each filter, updated every main loop.
        self._filter_results: Dict[str, Any] = {}

        # Setup server
        self._serve = bool(self._args.server)
        self._host = self._args.server.get("host", "localhost")
        self._port = self._args.server.get("port", 8765)
        self._ssl_ctx = create_ssl_context(self._args.server.get("ssl", ""))

        self._server_is_running: bool = False
        self._stop: bool = False

        if self._serve:
            self._server_thread: threading.Thread = threading.Thread(
                target=self.server_thread_func
            )
            self._server_thread.start()

        self._latest_received_data: Dict[str, Any] = {}
        self._latest_received_timestamps: Dict[str, float] = {}

    def latest_data(self) -> Optional[Any]:
        # Clear the latest received data from unresponsive clients
        keys_to_pop: List[str] = []
        for key, timestamp in self._latest_received_timestamps.items():
            if time.time() - timestamp > 10.0:
                keys_to_pop.append(key)
        for key in keys_to_pop:
            self._latest_received_data.pop(key)
            self._latest_received_timestamps.pop(key)

        return self._latest_received_data

    def wait_for_server_start(self, timeout: int = 0) -> bool:
        """Blocking function, does not return until the server is running. can
        be run with a timeout, in which case it will return if the server has
        not started before the timeout has expired.

        :param timeout: int - number of seconds after which to return, if the
        server has not started running by then.

        :return: bool - True if server is running. False if timeout reached
        without server started.
        """
        start_time = time.time()
        while not self._server_is_running:
            if timeout > 0 and time.time() > start_time + timeout:
                return False

        return True

    def wait_for_server_stop(self, timeout: int = 0) -> bool:
        """Blocking function, does not return until the server has stopped
        running. can be run with a timeout, in which case it will return if the
        server has not stopped before the timeout has expired.

        :param timeout: int - number of seconds after which to return, if the
        server has not stopped running by then.

        :return: bool - True if server has stopped. False if timeout reached
        without server stopped.
        """
        start_time = time.time()
        while self._server_is_running:
            if timeout > 0 and time.time() > start_time + timeout:
                return False

        return True

    def server_thread_func(self) -> None:
        """
        Function run in separate thread to start asyncio coroutine for
        websocket server. Websocket server sends out filter results
        continuously on a loop.
        """
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.server_coroutine())
        loop.close()
        self._server_is_running = False

    async def server_coroutine(self) -> None:
        """
        Asyncio coroutine to run websocket server inside asynchronous
        context manager.
        """
        async with serve(self.websocket_loop, self._host, self._port,
                         ssl=self._ssl_ctx):
            self._server_is_running = True
            while not self._stop:
                await asyncio.sleep(0.1)

    async def websocket_loop(self, websocket: WebSocketServerProtocol,
                             path: str) -> None:
        """Asyncio coroutine, runs a loop to continually send out filter
        results on websocket server. Exits when the websocket client
        disconnects, or if self.stop() is called.
        """
        logger.info("Websocket client connected!")

        while not self._stop:
            new_data = json.loads(await websocket.recv())
            self._latest_received_data.update(new_data)
            for key in new_data:
                self._latest_received_timestamps[key] = time.time()

    def stop(self) -> None:
        self._stop = True

        if self._serve:
            self._server_thread.join()
