import logging
import multiprocessing
import multiprocessing.dummy
import sys
import threading

import cv2
import numpy as np

from cProfile import Profile
from importlib import __import__, reload
from time import time, sleep

from livepose.camera import Camera
from livepose.config import Config
from livepose.pose_backend import PoseBackend
from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import Filter
from livepose.output import Output
from livepose.input import Input
from livepose.dimmap import DimMap

from typing import Dict, List, Optional, Set

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Context:
    """
    Base class to run a LivePose detection context
    """

    def __init__(self, name: str, config: Optional[Config]) -> None:
        self._name = name
        self._config = config
        self._cameras: Dict[str, Camera] = {}

        self._hidden = False
        self._show_fps = False
        self._verbose = False
        self._profile = False

        self._resize_frames = False
        self._is_ready = False
        self._framerate: float = 0.
        self._framerate_limit: int = 120

        self._flow: Flow = Flow()
        self._pose_backends: Set[PoseBackend] = set()
        self._dimmaps: Set[DimMap] = set()
        self._filters: Set[Filter] = set()
        self._inputs: List[Input] = []
        self._outputs: List[Output] = []

        self._thread = threading.Thread(target=self.update_loop)
        self._condition = threading.Condition()

        self._running = False
        self._frame_by_frame = False

    @property
    def configuration(self) -> Optional[Config]:
        """
        Get the current configuration as a dictionary

        :return: The configuration
        """
        return self._config

    @configuration.setter
    def configuration(self, config: Config) -> None:
        """
        Set the configuration from  a dictionary

        :param config: Configuration
        """
        self._is_ready = False

        self._pose_backends.clear()
        self._filters.clear()
        self._outputs.clear()

        self._config = config

        #
        # Grab extrinsic/intrinsic parameters if exist in config or after retrieval using camera apis
        #
        for path, camera in self._cameras.items():
            if camera.kmat is not None:
                self._config.set_kmat(path, camera.kmat)
            if camera.extrinsics is not None:
                self._config.set_extrinsics(path, camera.extrinsics)

        #
        # Regenerate kmat/extrinsics dicts from config for parsing by filters as arguments
        #
        kmat_dict = self._config.kmat_dict
        for camera_name, kmat in kmat_dict.items():
            kmat_dict[camera_name] = kmat.reshape(-1).tolist()
        extrinsics_dict = self._config.extrinsics_dict
        for camera_name, extrinsics in extrinsics_dict.items():
            extrinsics_dict[camera_name] = extrinsics.reshape(-1).tolist()

        #
        # Configure deep learning backend used to run inference on the images.
        #
        for pose_backend_desc in self._config.pose_backends:
            pose_backend_name = pose_backend_desc.get("name", "")
            if not pose_backend_name:
                continue

            pose_backend_path = f"livepose.pose_backends.{pose_backend_name}"
            try:
                reload(__import__(pose_backend_path))
            except ImportError as e:
                logger.error(
                    f"Could not import pose backend {pose_backend_name} from {pose_backend_path}: {e}.")
                sys.exit(1)

            pose_backend_params = pose_backend_desc.get("params", {})
            pose_backend = PoseBackend.create_from_name(
                name=pose_backend_name,
                **pose_backend_params
            )
            if pose_backend is None:
                logger.error(
                    f"Could not create pose backend {pose_backend_name}.")
                sys.exit(1)
            pose_backend.init()

            is_gpu_acceleration_needed = pose_backend.is_gpu_acceleration_needed()
            is_gpu_acceleration_active = pose_backend.is_gpu_acceleration_active()
            if is_gpu_acceleration_needed and not is_gpu_acceleration_active:
                other_backends = list(PoseBackend.registered_backends.keys())
                other_backends.remove(pose_backend_name)
                logger.error(
                    f"Deep Learning Model {pose_backend_name} requires GPU acceleration but GPU acceleration is not active. Check your installation or try another model among {other_backends}")
                return
            if is_gpu_acceleration_active:
                logger.info("GPU acceleration active")
            else:
                logger.warning("GPU acceleration not active, performance may be degraded")

            self._pose_backends.add(pose_backend)

        #
        # Configure dimensional mappers
        #
        for dimmap_name in self._config.dimmaps:
            dimmap_params = self._config.get_dimmap_params(dimmap_name)
            assert(dimmap_params is not None)

            dimmap_module_path = f"livepose.dimmaps.{dimmap_name}"
            try:
                reload(__import__(dimmap_module_path))
            except ImportError as e:
                logger.error(
                    f"Could not import dimmap module {dimmap_name} from {dimmap_module_path}: {e}.")
                sys.exit(1)

            new_dimmap = DimMap.create_from_name(name=dimmap_name, **dimmap_params)

            if new_dimmap is None:
                logger.error((
                    f"Dimension mapping type {dimmap_name} does not exist. "
                ))
                return
            new_dimmap.init()
            self._dimmaps.add(new_dimmap)

        #
        # Configure filters
        #
        for filter_name in self._config.filters:
            filter_params = self._config.get_filter_params(filter_name)
            assert(filter_params is not None)

            if self._config.kmat_dict and self._config.extrinsics_dict:
                # Retrieve Kmats/Rtmats from config when not specified as arguments for filter
                if 'Kmats' not in filter_params:
                    filter_params['Kmats'] = kmat_dict
                if 'Rtmats' not in filter_params:
                    filter_params['Rtmats'] = extrinsics_dict

            filter_module_path = f"livepose.filters.{filter_name}"
            try:
                reload(__import__(filter_module_path))
            except ImportError as e:
                logger.error(
                    f"Could not import filter module {filter_name} from {filter_module_path}: {e}.")
                sys.exit(1)

            new_filter = Filter.create_from_name(name=filter_name, **filter_params)

            if new_filter is None:
                logger.error((
                    f"Filter type {filter_name} does not exist. "
                ))
                return
            new_filter.init()
            self._filters.add(new_filter)

        #
        # Configure inputs
        #
        for input_name in self._config.inputs:
            input_params = self._config.get_input_params(input_name)
            assert(input_params is not None)
            input_module_path = f"livepose.inputs.{input_name}"
            try:
                reload(__import__(input_module_path))
            except ImportError:
                logger.error(
                    f"Could not import input module {input_name} from {input_module_path}.")
                sys.exit(1)

            new_input = Input.create_from_name(
                name=input_name,
                **input_params
            )
            if new_input is None:
                logger.error((
                    f"Input type {input_name} does not exist. "
                ))
                return
            new_input.init()
            self._inputs.append(new_input)

        #
        # Configure outputs
        #
        for output_name in self._config.outputs:
            output_params = self._config.get_output_params(output_name)
            assert(output_params is not None)

            output_module_path = f"livepose.outputs.{output_name}"
            try:
                reload(__import__(output_module_path))
            except ImportError as e:
                logger.error(
                    f"Could not import output module {output_name} from {output_module_path}: {e}.")
                sys.exit(1)

            new_output = Output.create_from_name(
                name=output_name,
                **output_params
            )
            if new_output is None:
                logger.error((
                    f"Output type {output_name} does not exist. "
                ))
                return
            new_output.init()
            self._outputs.append(new_output)

        self._is_ready = True

    @property
    def is_ready(self) -> bool:
        """
        Get whether this context is ready to run

        :return: Return the readiness
        """
        return self._is_ready

    @property
    def framerate(self) -> float:
        """
        Get the current framerate for the context

        :return: The framerate
        """
        return self._framerate

    @property
    def framerate_limit(self) -> int:
        """
        Get the framerate limit when no camera is configured

        :return: The framerate limit
        """
        return self._framerate_limit

    @framerate_limit.setter
    def framerate_limit(self, limit: int) -> None:
        """
        Set the framerate limit when no camera is configured
        """
        if limit < 0:
            return

        self._framerate_limit = limit

    @property
    def hidden(self) -> bool:
        """
        Get whether this context can display windows or not
        :return: Return True if it can show windows
        """
        return self._hidden

    @hidden.setter
    def hidden(self, hidden: bool) -> None:
        """
        Set whether this context can show windows or not

        :param hidden: Hidden if True
        """
        self._hidden = hidden

    @property
    def running(self) -> bool:
        """
        Get whether the context is running

        :return: True if running
        """
        return self._running

    @property
    def profile(self) -> bool:
        """
        Get whether to profile the context
        :return: Return True if profiling is enabled
        """
        return self._profile

    @profile.setter
    def profile(self, profile: bool) -> None:
        """
        Set whether this context should be profiled

        :param profile: Profile if True
        """
        self._profile = profile

    @property
    def show_fps(self) -> bool:
        """
        Get whether to show frame per seconds or not
        :return: Return True if it can show windows
        """
        return self._show_fps

    @show_fps.setter
    def show_fps(self, show_fps: bool) -> None:
        """
        Set whether this context should show frames per seconds

        :param show_fps: FPS shown if True
        """
        self._show_fps = show_fps

    @property
    def verbose(self) -> bool:
        return self._verbose

    @verbose.setter
    def verbose(self, verbose: bool) -> None:
        self._verbose = verbose

    def initialize_cameras(self) -> bool:
        assert(self._config is not None)

        for source in self._config.cam_paths:
            params = self._config.cam_params.get(source, {})
            params["path"] = source

            camera_module_name = params.get("api", "opencv")
            camera_module_path = f"livepose.cameras.{camera_module_name}"
            try:
                reload(__import__(camera_module_path))
            except ImportError as e:
                logger.error(
                    f"Could not import camera module {camera_module_name} from {camera_module_path}: {e}.")
                sys.exit(1)

            cam: Optional[Camera] = Camera.create_from_name(
                name=camera_module_name,
                **params
            )
            if cam is None:
                logger.error(f"Could not create camera module {camera_module_name}.")
                sys.exit(1)
            cam.init()

            if cam.kmat is None:
                cam.kmat = self._config.kmat_dict.get(source, None)
            if cam.dist_coeffs is None:
                cam.dist_coeffs = self._config.dist_coeffs_dict.get(source, None)
            if cam.extrinsics is None:
                cam.extrinsics = self._config.extrinsics_dict.get(source, None)

            if cam.is_open():
                cam_resolution = tuple(params.get("resolution", ()))
                cam_framerate = params.get("framerate", None)

                # Change resolution if flag provided
                if len(cam_resolution) == 2:
                    cam.resolution = cam_resolution  # type: ignore # because tuple length depends on config
                    # Make sure the resolution is properly set
                    if not cam_resolution == cam.resolution:
                        self._resize_frames = True
                        logger.warning(f"Unable to set the resolution to the desired value for camera ID: {source}."
                                       f"\nPlease make sure the resolution is supported by the device"
                                       f"\nResizing each frame to {cam_resolution} instead.")
                if cam_framerate:
                    cam.fps = cam_framerate  # type: ignore
                    if not cam_framerate == cam.fps:
                        logger.warning(f"Unable to set the framerate to the desired value for camera ID: {source}.")
                self._cameras[source] = cam
            else:
                self._config.cam_paths.remove(source)

        if not self._cameras:
            logger.error("Unable to open any camera")

        return True

    def retrieve_camera_frames(self, flow: Flow) -> bool:
        """
        Retrieve the last frames grabbed from the cameras

        :return: Whether the last camera was just closed
        """
        if not self._cameras:
            return True

        assert(self._config is not None)
        assert(self._thread_pool is not None)

        # when the cameras do not have hardware synchronization, we should call
        # VideoCapture::grab() for each camera and after that call the slower
        # method VideoCapture::retrieve() to decode and get frame from each camera
        # see https://docs.opencv.org/3.4/d8/dfe/classcv_1_1VideoCapture.html#ae38c2a053d39d6b20c9c649e08ff0146

        # Grab the frames for the cameras
        self._thread_pool.map(Camera.grab_all, self._cameras.values())

        paths_to_remove: List[str] = []

        for path, camera in self._cameras.items():
            if not camera.retrieve():
                if not camera.is_open():
                    logger.info(f"source {path} closed")
                    paths_to_remove.append(path)
                continue
            else:
                channels = camera.get_channels()

                for c in range(len(channels)):
                    if channels[c].type is Channel.Type.COLOR:
                        for f in range(len(channels[c].data)):
                            frame = channels[c].data[f]

                            # Resize if resolution could not be enforced
                            if self._resize_frames:
                                resolution = tuple(self._config.cam_params.get(path, {}).get("resolution", None))
                                assert(resolution)
                                frame = cv2.resize(frame, resolution)

                            # if distortion coefficients and camera matrix are provided, remove distortion from the image
                            if camera.kmat is not None and camera.dist_coeffs is not None:
                                frame = cv2.undistort(frame, camera.kmat, camera.dist_coeffs)

                            channels[c].data[f] = frame

                if not flow.has_stream(path):
                    flow.add_stream(name=path, type=Stream.Type.INPUT)
                flow.set_stream_frame(name=path, frame=channels)

        if len(paths_to_remove) > 0:
            for path in paths_to_remove:
                logger.info(f"remove closed source {path}")
                del self._cameras[path]
                if not len(self._cameras):
                    return False
        return True

    def draw_fps(self, fps: float) -> np.array:
        """
        Utility function to draw the fps on a mask that will be merged to the tracked image
        :param: float - computed fps
        :return: np.array - mask with the fps text
        """
        text = f"FPS: {fps}"
        (text_width, text_height) = cv2.getTextSize(text, cv2.FONT_HERSHEY_DUPLEX, 1.0, 1)[0]
        text_height += 15  # this is to have some offset
        mask = np.zeros((text_height, text_width), dtype=np.uint8)

        mask = cv2.putText(mask, text, (0, text_height - 5), cv2.FONT_HERSHEY_DUPLEX,
                           1, (255, 255, 255), 1, cv2.LINE_AA)
        mask = cv2.merge((mask, mask, mask))  # we need 3 channels
        return mask

    def update_loop(self) -> None:
        if self._config is None:
            logger.error("One config file need to be passed")
            return

        self.configuration = self._config

        if not self.initialize_cameras():
            self._running = False
            return

        # Initialize the thread pool
        if self._cameras:
            self._thread_pool = multiprocessing.dummy.Pool(len(self._cameras))

        # Start the various pose backends
        for pose_backend in self._pose_backends:
            if not pose_backend.start():
                logger.error("Unable to start deep learning backend")
                return

        start_time = time()
        previously = 0.0

        if self._profile:
            profiler = Profile()
            profiler.enable()
            logger.info("Profiling enabled")

        input_available = True
        while (self._running and input_available):
            self._flow.next()

            now = time() - start_time
            dt = now - previously

            for input in self._inputs:
                input.retrieve_flows(self._flow, now, dt)

            if self._verbose:
                print(f"\nCurrent frame: time = {now}, delta = {dt}")

            # Retrieve grabbed frames
            input_available = self.retrieve_camera_frames(self._flow)
            if (not input_available):
                logger.info("no input available, closing.")
                break

            # Apply pose detection methods
            for pose_backend in self._pose_backends:
                res = pose_backend.step(flow=self._flow, now=now, dt=dt)
                if not res and self._cameras:
                    logger.error("Deep learning model failed to process image.")
                    break

                # If LivePose is not hidden, ask for drawing the objects
                if not self.hidden:
                    pose_backend.draw_objects()

                pose_backend_stream_name = f"pose_backend_{type(pose_backend).__name__}"

                if not self._flow.has_stream(pose_backend_stream_name):
                    self._flow.add_stream(
                        name=pose_backend_stream_name,
                        type=Stream.Type.POSE_BACKEND
                    )

                self._flow.set_stream_frame(
                    name=pose_backend_stream_name,
                    frame=pose_backend.get_output()
                )

            # Extract more dimensional information
            for dimmap in self._dimmaps:
                dimmap.step(
                    flow=self._flow,
                    now=now,
                    dt=dt
                )

            # Apply filters
            for filter in self._filters:
                filter.step(
                    flow=self._flow,
                    now=now,
                    dt=dt
                )

            # Send results
            for output in self._outputs:
                output.send_flow(self._flow, now, dt)

            # Update framerate
            self._framerate = round(self._framerate * 0.9 + 1.0 / (now - previously) * 0.1, 2)

            # Print flow in the terminal if verbose
            if self._verbose:
                print(self._flow.trace())

            # Show result on screen
            if not self.hidden:
                pose_streams = self._flow.get_streams_by_type(Stream.Type.POSE_BACKEND)
                if not pose_streams:
                    pose_streams = self._flow.get_streams_by_type(Stream.Type.INPUT)
                for stream in pose_streams.values():
                    color_channels = stream.get_channels_by_type(Channel.Type.COLOR)

                    for index, color_channel in enumerate(color_channels):
                        tracked_image = color_channel.data
                        if self.show_fps:
                            mask = self.draw_fps(self._framerate)
                            shape = mask.shape
                            # make sure we can display the fps
                            if np.all(np.less_equal(shape, tracked_image.shape)):
                                tracked_image[-shape[0]:, -shape[1]:, :] = cv2.bitwise_or(
                                    tracked_image[-shape[0]:, -shape[1]:, :],
                                    mask
                                )
                            else:
                                logger.warning("Unable to display the FPS, frame resolution too small")

                        cv2.imshow(f"LivePose - {stream.name}:{index} - {self._name}", tracked_image)
                        cv2.waitKey(1)

            if self._frame_by_frame:
                with self._condition:
                    self._condition.wait()

            if not self._cameras:
                # if no camera is present, we limit artificially the framerate to self._framerate_limit
                frame_min_duration = 1.0 / self._framerate_limit
                frame_duration = time() - (start_time + now)

                if frame_duration < frame_min_duration:
                    sleep(frame_min_duration - frame_duration)

            previously = now

        if self._profile:
            profiler.disable()
            profiler.create_stats()
            profile_filename = "profiling.prof"
            profiler.dump_stats(profile_filename)
            logger.info(f"Profiling data dumped into {profile_filename}")

        for input in self._inputs:
            input.stop()

        for output in self._outputs:
            output.stop()

        if self._cameras:
            self._thread_pool.close()

    def run(self, frame_by_frame: bool = False) -> None:
        """
        Run the context asynchronously

        :param frame_by_frame: If True, runs the context in frame-by-frame mode
        """

        if self._config is None:
            return

        self._running = True
        self._frame_by_frame = frame_by_frame
        self._thread.start()

    def step(self) -> None:
        """
        Process the next frame, if the context runs in frame-by-frame mode
        """

        with self._condition:
            self._condition.notifyAll()

    def stop(self) -> None:
        """
        Stop the context if running
        """

        if self._thread.is_alive():
            self._running = False
            with self._condition:
                self._condition.notifyAll()
            self._thread.join()
