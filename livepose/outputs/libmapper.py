import logging
import io
import sys
from typing import Any, Dict, Set

from livepose.dataflow import Channel, Flow, Stream
from livepose.output import register_output, Output
from livepose.filter import Filter

# import libmapper without prints
stdout = sys.stdout
sys.stdout = io.StringIO()
import libmapper as mpr  # noqa
# get output
module_output = sys.stdout.getvalue()
# restore sys.stdout
sys.stdout = stdout


logger = logging.getLogger(__name__)


@register_output("libmapper")
class MapperOutput(Output):
    """
    Class which sends out filter results via libmapper.
    """

    def __init__(self, **kwargs: Any):
        super(MapperOutput, self).__init__("libmapper", **kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()

    def init(self) -> None:

        logger.info(f"{module_output}")

        # libmapper producer device
        self._dev = mpr.Device("livepose")

    def send_flow(self, flow: Flow, now: float, dt: float) -> None:
        """
        """
        output_streams = flow.get_streams_by_type(Stream.Type.FILTER)

        for name, stream in output_streams.items():
            channels = stream.get_channels_by_type(Channel.Type.OUTPUT)

            for channel in channels:
                results: Dict[str, Any] = channel.data

                for path, result in results.items():
                    # Add/get signal with/from filter result path
                    if result is not None:

                        # Cast result to array of floats
                        buffer = []
                        for i, r in enumerate(result):
                            buffer.append(float(r))
                        length = len(buffer)

                        signal = self._dev.add_signal(mpr.Direction.OUTGOING, str(
                            path), length, mpr.Type.FLOAT, None, None, None)

                        # Unpack buffer if single value
                        if length == 1:
                            signal.set_value(buffer[0])
                        else:
                            signal.set_value(buffer)

        # Poll devices (non-blocking)
        self._dev.poll(0)
