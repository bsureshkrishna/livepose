import liblo

from typing import Any, Dict, List, Set, Tuple, Union

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import Filter
from livepose.output import register_output, Output

BASE_OSC_PATH = "/livepose"


@register_output("osc")
class OSCOutput(Output):
    """
    Class which sends out filter results via OSC.
    :param destinations: Dictionnary (network address and port) of destinations
        where the OSC message will be sent to.
    """

    def __init__(self, **kwargs: Any):
        super(OSCOutput, self).__init__("osc", **kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()
        self._parser.add_argument("--destinations", type=Dict[str, int], default={},  # type: ignore
                                  help="Dictionnary (network address and port) of destinations")

    def init(self) -> None:
        pass

    def send_value(self, address: Tuple[str, str], path: str, name: str, value: Union[str, int, float]) -> None:
        """
        """
        liblo.send(address, f"{path}/{name}", value)

    def send_dict(self, address: Tuple[str, str], path: str, name: str, values: Filter.Result) -> None:
        """
        """
        new_path = f"{path}/{name}"

        for value_name, value in values.items():
            if type(value) == dict:
                self.send_dict(address, new_path, str(value_name), value)
            elif type(value) == list:
                self.send_list(address, new_path, str(value_name), value)
            else:
                self.send_value(address, new_path, str(value_name), value)

    def send_list(self, address: Tuple[str, str], path: str, name: str, value: List[Any]) -> None:
        """
        """
        liblo.send(address, f"{path}/{name}", *value)

    def send_flow(self, flow: Flow, now: float, dt: float) -> None:
        """
        """
        for destination, port in self._args.destinations.items():
            address = (destination, str(port))
            liblo.send(address, f"{BASE_OSC_PATH}/frame", now, dt)
            output_streams = flow.get_streams_by_type(Stream.Type.FILTER)

            for stream in output_streams.values():
                liblo.send(address, f"{BASE_OSC_PATH}/{stream.name}/start_filter")
                assert(stream is not None)
                channels = stream.get_channels_by_type(Channel.Type.OUTPUT)
                for channel in channels:

                    results: Filter.Result = channel.data
                    self.send_dict(address, f"{BASE_OSC_PATH}", stream.name, results)

                liblo.send(address, f"{BASE_OSC_PATH}/{stream.name}/end_filter")

            liblo.send(address, f"{BASE_OSC_PATH}/end_frame", now, dt)
