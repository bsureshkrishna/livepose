import logging
import argparse

import numpy as np
import sys

from abc import ABCMeta, abstractmethod
from operator import itemgetter
from typing import Any, Callable, Dict, List, Optional, Type

from dataclasses import dataclass

from livepose.extlib.sort import Sort
from livepose.dataflow import Channel, Flow, Stream
from livepose.node import Node

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def register_pose_backend(name: str) -> Callable:
    """
    Decorator for registering backends
    :param name: name - Name of the backend
    """

    def deco(cls: Type['PoseBackend']) -> Callable:
        PoseBackend.register(backend_type=cls, name=name)
        return cls

    return deco


@dataclass
class Keypoint:
    confidence: float
    part: str  # Name of the keypoint
    position: List[float]  # Can be 2D or 3D coordinates


@dataclass
class Pose:
    confidence: float
    id: int
    keypoints: Dict[str, Keypoint]
    keypoints_definitions: Dict[str, int]


@dataclass
class PosesForCamera:
    poses: List[Pose]


class PoseBackend(Node, metaclass=ABCMeta):
    """Generic backend for loading and running different ML models."""

    registered_backends: Dict[str, Type['PoseBackend']] = dict()

    def __init__(self, **kwargs: Any) -> None:
        """Init DL Backend class."""
        super().__init__(**kwargs)

        self._poses_for_cameras: List[PosesForCamera] = []
        self._model_path: Optional[str] = None
        self._bounding_boxes: Optional[np.ndarray] = None
        self._tracked_images: List[np.ndarray] = []
        self._object_trackers: List[Sort] = []

    @abstractmethod
    def add_parameters(self) -> None:
        """Add DL Backend parameters."""
        super().add_parameters()
        self._parser.add_argument('--compact-track-ids', type=bool, default=False,
                                  help="True if tracked IDs must start at 0 (with no gap), False if tracked IDs must be increase with people going out/into the camera frame.")

    @abstractmethod
    def init(self) -> None:
        """Init DL Backend models and objects."""
        pass

    @classmethod
    def register(cls, backend_type: Type['PoseBackend'], name: str) -> None:
        if name not in cls.registered_backends:
            cls.registered_backends[name] = backend_type
        else:
            raise Exception(
                f"A DL backend type {name} has already been registered")

    @classmethod
    def create_from_name(cls, name: str, **kwargs: Any) -> Optional['PoseBackend']:
        if name not in PoseBackend.registered_backends:
            return None
        else:
            return PoseBackend.registered_backends[name](**kwargs)

    def initialize_object_trackers(self, num_cams: int, **kwargs: Any):
        """Initialize SORT Object Tracker objects for each camera. The object
        trackers maintain unique IDs for each detected pose instance from frame
        to frame.

        :param num_cams: Number of cameras in use. Equal to the number of
        object trackers that will be initialized and returned.

        :return List[Sort]: Return a list of SORT Object Trackers, one for each
        camera.
        """
        if num_cams == len(self._object_trackers):
            return

        self._object_trackers = [Sort(**kwargs) for _ in range(num_cams)]

    def update_object_trackers(self, bounding_boxes: List[np.ndarray]
                               ) -> List[np.ndarray]:
        """Update object trackers and assign tracking IDs to bounding boxes for
        each camera. The tracking IDs are maintained between frames and are
        uniquely assigned to each detected bounding box.

        For each camera, an array of shape (n, 6) is returned, where n is the
        number of tracked IDs. n may be more or less than the number of input
        bounding boxes, if a new person has just entered or exited the frame
        for example.

        For each bounding box in the output, the first 4 elements are the
        updated coordinates (x1, y1, x2, y2). The last 2 elements are the index
        within the input and the tracking ID, respectively. The input index is
        used to corrolate the input and output bounding boxes, since the output
        array is not guaranteed to be in the same order as the input
        (there may be elements added or missing, from trackers leftover from
        previous frames for example).

        Example: [x1, y1, x2, y2, 0, 1] represents the bounding box at index 0
        in the input, assigned tracking ID 1.

        :param bounding_boxes: List of bounding boxes for each camera. Each
        camera should contain a numpy array of bounding boxes of shape (m, 5).
        The first 4 elements of each bounding box are x1, y1, x2, y2, and the
        last element is the detection confidence score.

        :return List[np.ndarray]: Similar list of numpy arrays for each camera,
        each with shape (n, 6). For each bounding box, the first 4 elements are
        the bounding box coordinates, and the last 2 elements are the input
        index and tracking ID, respectively.
        """
        # Contains bounding boxes but with the last column being the unique ID.
        tracked_bboxes = []
        for i, bbox in enumerate(bounding_boxes):
            tracked_bbox = self._object_trackers[i].update(bbox)
            tracked_bboxes.append(tracked_bbox)

        return tracked_bboxes

    def get_bounding_boxes(self) -> List[np.array]:
        """
        Get coordinates for bounding boxes containing each pose detected in
        each camera image.
        """
        bounding_boxes = []
        for poses_for_camera in self._poses_for_cameras:
            bounding_boxes_for_cam = []
            for pose in poses_for_camera.poses:
                keypoints = pose.keypoints.values()

                # Do not keep empty poses
                valid_keypoints = np.array([[kp.position[0], kp.position[1], kp.confidence]
                                            for kp in keypoints if kp.confidence > 0.0])
                if not len(valid_keypoints):
                    continue

                confidence = np.mean(valid_keypoints[:, 2])

                min_x = np.min(valid_keypoints[:, 0])
                min_y = np.min(valid_keypoints[:, 1])
                max_x = np.max(valid_keypoints[:, 0])
                max_y = np.max(valid_keypoints[:, 1])

                bounding_boxes_for_cam.append(np.array([
                    min_x, min_y, max_x, max_y, confidence
                ]))

            if bounding_boxes_for_cam:
                bounding_boxes.append(np.array(bounding_boxes_for_cam))
            else:
                # NOTE: The SORT Object Tracker requires an array with shape
                # (n, 5) for every frame, even if there are no detections.
                bounding_boxes.append(np.empty((0, 5)))

        return bounding_boxes

    @property
    def poses_for_cameras(self) -> List[PosesForCamera]:
        """
        Get list of keypoint dictionaries.

        :return: List - list poses detected by a camera, indexed by camera id
        """
        return self._poses_for_cameras

    @property
    def model_path(self) -> Optional[str]:
        """Get the path to the Deep Learning model(s).
        :return: str - Path to the model(s).
        """
        return self._model_path

    @model_path.setter
    def model_path(self, path: str) -> None:
        """Set the path to the Deep Learning model(s).
        :param str:  Path to the model
        """
        self._model_path = path

    @property
    def bounding_boxes(self) -> Optional[np.ndarray]:
        """Get bounding boxes for last frame processed, indexed by camera.
        :return: np.ndarray - image
        """
        return self._bounding_boxes

    @property
    def tracked_images(self) -> List[np.array]:
        """Get image currently being processed.
        :return: List[np.array] - image
        """
        return self._tracked_images

    def draw_objects(self) -> None:
        """
        Draw detected objects onto the source image
        """
        pass

    def start(self) -> bool:
        """Start the DL Backend model.
        :return: bool - success of start. Returns False if there is no valid
        image generator, model path, etc.
        """
        if self._model_path is None:
            return False

        return True

    def stop(self) -> None:
        """Stop DL Backend model."""
        pass

    def step(self, flow: Flow, now: float, dt: float) -> bool:
        """
        Process the given image(s) with the DL Backend model.
        This method should not be overloaded by the pose backends, which
        should instead define their own step_pose_detection method.
        :param flow: Flow - Data flow to read from and write to
        :param now: Current time
        :param dt: Time since last call
        """
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)
        num_streams = len([True for stream in input_streams.values(
        ) if Channel.Type.COLOR in stream.channel_types])
        self.initialize_object_trackers(num_streams)

        res = self.step_pose_detection(flow, now, dt)
        if not res:
            return False

        # Compute the bounding boxes, and do the tracking
        bounding_boxes = self.get_bounding_boxes()
        self._bounding_boxes = self.update_object_trackers(bounding_boxes)

        # Adjust the pose ids
        for cam_index, camera in enumerate(self._bounding_boxes):
            associations = []
            for bbox in camera:
                source_id = int(bbox[4])
                dest_id = int(bbox[5])
                associations.append([source_id, dest_id])

            if self._args.compact_track_ids:
                associations = sorted(associations, key=itemgetter(1))
                for index, association in enumerate(associations):
                    association[1] = index

            for source_id, dest_id in associations:
                if source_id > len(self._poses_for_cameras):
                    continue

                self._poses_for_cameras[cam_index].poses[source_id].id = dest_id

        return True

    @abstractmethod
    def step_pose_detection(self, flow: Flow, now: float, dt: float) -> bool:
        """
        Step through the pose detection
        :param flow: Flow - Data flow to read from and write to
        :param now: Current time
        :param dt: Time since last call
        """
        pass

    @abstractmethod
    def get_output(self) -> List[Channel]:
        """
        Get the list of output channels for this backend
        :return: List[Channel] - List of output channels
        """
        pass

    @abstractmethod
    def is_gpu_acceleration_active(self) -> bool:
        """Check if GPU acceleration is active."""
        pass

    @abstractmethod
    def is_gpu_acceleration_needed(self) -> bool:
        """Check if GPU acceleration is needed."""
        pass
