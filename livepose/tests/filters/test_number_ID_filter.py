"""Unit tests for livepose/filters/number_ID.py"""

from unittest import TestCase

from livepose.dataflow import Channel, Flow, Stream
from livepose.filters.number_id import NumberIDFilter
from livepose.keypoints.body_25 import Body25Keypoints
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


class TestStep(TestCase):
    def setUp(self):
        self.filter = NumberIDFilter()
        self.filter.init()

    def test_no_keypoints(self):
        """Test if no keypoints are provided, filter.result is empty"""
        self.assertEqual(self.filter.result, {})

        self.filter.step(
            flow=Flow(),
            now=None,
            dt=None
        )

        self.assertEqual(self.filter.result, {})

    def test_multiple_cameras(self):
        """Test filter.result contains the correct value for each
        separate camera.
        """
        pose_1 = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 1])
            },
            keypoints_definitions=Body25Keypoints
        )

        pose_2 = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 1])
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_1]), PosesForCamera(poses=[pose_2])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=None,
            dt=None
        )

        self.assertEqual(self.filter.result[0], 1)
        self.assertEqual(self.filter.result[1], 1)

    def test_multiple_skeletons(self):
        """Test filter.result contains the correct value for each
        separate skeleton attached to a single camera.
        """
        pose_1 = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=1.0, part="NOSE", position=[1, 1])
            },
            keypoints_definitions=Body25Keypoints
        )

        pose_2 = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=1.0, part="NOSE", position=[1, 1])
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_1, pose_2])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=None,
            dt=None
        )

        self.assertEqual(self.filter.result[0], 2)
