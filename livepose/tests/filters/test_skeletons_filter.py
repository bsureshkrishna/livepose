"""Unit tests for livepose/filters/skeletons.py"""

from unittest import TestCase

import numpy as np

from livepose.dataflow import Channel, Flow, Stream
from livepose.keypoints.body_25 import Body25Keypoints
from livepose.filters.skeletons import SkeletonsFilter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


class TestStep(TestCase):
    def setUp(self):
        self.filter = SkeletonsFilter()
        self.filter.init()

        self.base_path = f"{self.filter._filter_name}"

    def test_no_poses(self):
        """Test if no keypoints for any cameras are provided, filter.result is
        an empty dict."""
        self.assertEqual(self.filter.result, {})

        self.filter.step(
            flow=Flow(),
            now=None,
            dt=None
        )

        self.assertEqual(self.filter.result, {})

    def test_one_pose(self):
        """Test pose ID and keypoints for a single pose and a single camera are
        copied correctly to filter.result
        """
        self.assertEqual(self.filter.result, {})

        pose = Pose(confidence=1.0, id=0, keypoints={}, keypoints_definitions=Body25Keypoints)

        for i, keypoint in enumerate(Body25Keypoints):
            pose.keypoints[keypoint] = Keypoint(confidence=1.0, part=keypoint, position=(i * 2, i * 3))

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=None,
            dt=None
        )

        self.assertEqual(self.filter.result[0][0]["pose_id"], 0)

        for i, keypoint in enumerate(Body25Keypoints):
            self.assertEqual(
                self.filter.result[0][0]["keypoints"][keypoint],
                [i * 2, i * 3, 1.0]
            )

    def test_one_camera_multiple_poses(self):
        """Test keypoints for multiple poses detected in a single camera
        are copied correctly to filter.result
        """
        self.assertEqual(self.filter.result, {})

        pose_1 = Pose(confidence=1.0, id=1, keypoints={}, keypoints_definitions=Body25Keypoints)
        pose_2 = Pose(confidence=1.0, id=2, keypoints={}, keypoints_definitions=Body25Keypoints)

        for i, keypoint in enumerate(Body25Keypoints):
            pose_1.keypoints[keypoint] = Keypoint(
                confidence=1.0,
                part=keypoint,
                position=(i * 2, i * 3)
            )

            pose_2.keypoints[keypoint] = Keypoint(
                confidence=1.0,
                part=keypoint,
                position=(i * 7, i * 11)
            )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_1, pose_2])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=None,
            dt=None
        )

        for i in range(2):
            self.assertEqual(self.filter.result[0][i]["pose_id"], i + 1)

        for i, keypoint in enumerate(Body25Keypoints):
            self.assertEqual(
                self.filter.result[0][0]["keypoints"][keypoint],
                [i * 2, i * 3, 1.0]
            )

            self.assertEqual(
                self.filter.result[0][1]["keypoints"][keypoint],
                [i * 7, i * 11, 1.0]
            )

    def test_multiple_cameras_one_pose(self):
        """Test keypoints for multiple cameras each with one pose are
        copied correctly to filter.result
        """
        self.assertEqual(self.filter.result, {})

        pose_1 = Pose(confidence=1.0, id=1, keypoints={}, keypoints_definitions=Body25Keypoints)
        pose_2 = Pose(confidence=1.0, id=2, keypoints={}, keypoints_definitions=Body25Keypoints)

        for i, keypoint in enumerate(Body25Keypoints):
            pose_1.keypoints[keypoint] = Keypoint(
                confidence=1.0,
                part=keypoint,
                position=(i * 2, i * 3)
            )

            pose_2.keypoints[keypoint] = Keypoint(
                confidence=1.0,
                part=keypoint,
                position=(i * 7, i * 11)
            )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_1]), PosesForCamera(poses=[pose_2])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=None,
            dt=None
        )

        for i in range(2):
            self.assertEqual(self.filter.result[i][0]["pose_id"], i + 1)

        for i, keypoint in enumerate(Body25Keypoints):
            self.assertEqual(
                self.filter.result[0][0]["keypoints"][keypoint],
                [i * 2, i * 3, 1.0]
            )

            self.assertEqual(
                self.filter.result[1][0]["keypoints"][keypoint],
                [i * 7, i * 11, 1.0]
            )

    def test_multiple_cameras_multiple_poses(self):
        """Test keypoints for multiple cameras each with multiple poses
        are correctly copied to filter.result
        """
        self.assertEqual(self.filter.result, {})

        pose_1 = Pose(confidence=1.0, id=1, keypoints={}, keypoints_definitions=Body25Keypoints)
        pose_2 = Pose(confidence=1.0, id=2, keypoints={}, keypoints_definitions=Body25Keypoints)
        pose_3 = Pose(confidence=1.0, id=3, keypoints={}, keypoints_definitions=Body25Keypoints)
        pose_4 = Pose(confidence=1.0, id=4, keypoints={}, keypoints_definitions=Body25Keypoints)

        for i, keypoint in enumerate(Body25Keypoints):
            pose_1.keypoints[keypoint] = Keypoint(confidence=1.0, part=keypoint, position=(i * 2, i * 3))
            pose_2.keypoints[keypoint] = Keypoint(confidence=1.0, part=keypoint, position=(i * 7, i * 11))
            pose_3.keypoints[keypoint] = Keypoint(confidence=1.0, part=keypoint, position=(i * 17, i * 19))
            pose_4.keypoints[keypoint] = Keypoint(confidence=1.0, part=keypoint, position=(i * 29, i * 31))

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_1, pose_2]), PosesForCamera(poses=[pose_3, pose_4])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=None,
            dt=None
        )

        for i in range(2):
            self.assertEqual(self.filter.result[0][i]["pose_id"], i + 1)

        for i in range(2):
            self.assertEqual(self.filter.result[1][i]["pose_id"], i + 3)

        for i, keypoint in enumerate(Body25Keypoints):
            self.assertEqual(
                self.filter.result[0][0]["keypoints"][keypoint],
                [i * 2, i * 3, 1.0]
            )

            self.assertEqual(
                self.filter.result[0][1]["keypoints"][keypoint],
                [i * 7, i * 11, 1.0]
            )

            self.assertEqual(
                self.filter.result[1][0]["keypoints"][keypoint],
                [i * 17, i * 19, 1.0]
            )

            self.assertEqual(
                self.filter.result[1][1]["keypoints"][keypoint],
                [i * 29, i * 31, 1.0]
            )

    def test_unused_keypoints(self):
        """Test keypoints not given are not included in filter.result"""
        self.assertEqual(self.filter.result, {})

        pose = Pose(confidence=1.0, id=1, keypoints={}, keypoints_definitions=Body25Keypoints)

        pose.keypoints[list(Body25Keypoints)[6]] = Keypoint(
            confidence=1.0, part=list(Body25Keypoints)[6], position=(10., 12.))

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=None,
            dt=None
        )

        self.assertEqual(
            self.filter.result[0][0]["pose_id"],
            1
        )

        for i, keypoint in enumerate(Body25Keypoints):
            if i == 6:
                self.assertEqual(
                    self.filter.result[0][0]["keypoints"][keypoint],
                    [10., 12., 1.0]
                )
            else:
                self.assertIsNone(self.filter.result[0][0]["keypoints"].get(keypoint))

    def test_only_pose_id_no_keypoints(self):
        """Test if only the `pose_id` attribute is provided with no keypoints,
        `pose_id` is copied to filter.result while filter.result keypoints are
        each set to None.
        """
        self.assertEqual(self.filter.result, {})

        poses_for_cameras = [PosesForCamera(
            [
                Pose(confidence=1.0, id=1, keypoints={}, keypoints_definitions=Body25Keypoints),
                Pose(confidence=1.0, id=2, keypoints={}, keypoints_definitions=Body25Keypoints)
            ]),
            PosesForCamera(
            [
                Pose(confidence=1.0, id=3, keypoints={}, keypoints_definitions=Body25Keypoints),
                Pose(confidence=1.0, id=4, keypoints={}, keypoints_definitions=Body25Keypoints)
            ])
        ]

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=poses_for_cameras,
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=None,
            dt=None
        )

        for i in range(2):
            self.assertEqual(self.filter.result[0][i]["pose_id"], i + 1)

        for i in range(2):
            self.assertEqual(self.filter.result[1][i]["pose_id"], i + 3)

        for i, keypoint in enumerate(Body25Keypoints):
            self.assertIsNone(self.filter.result[0][0]["keypoints"].get(keypoint))
            self.assertIsNone(self.filter.result[0][1]["keypoints"].get(keypoint))
            self.assertIsNone(self.filter.result[1][0]["keypoints"].get(keypoint))
            self.assertIsNone(self.filter.result[1][1]["keypoints"].get(keypoint))
