"""Unit tests for Context class"""

from unittest import TestCase

from livepose.config import Config
from livepose.context import Context

DEFAULT_CONFIG_PATH = "default.json"


class TestContext(TestCase):
    def setUp(self):
        config = Config(DEFAULT_CONFIG_PATH)
        self.context = Context("Test context", config)

    def test_config(self):
        config = Config(DEFAULT_CONFIG_PATH)
        self.context.configuration = config

    def test_empty_run(self):
        self.context.run()
        self.context.stop()
