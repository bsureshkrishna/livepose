import sys
import numpy as np

from typing import Any, Dict, List, Union

from livepose.cameras.pyrealsense2 import PyRealSense2Camera
from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint

import logging

logger = logging.getLogger(__name__)

try:
    import pyrealsense2 as rs
    has_pyrealsense2 = True
except (ImportError, ModuleNotFoundError) as e:
    has_pyrealsense2 = False


@register_filter("position")
class PositionFilter(Filter):
    """
    Filter which outputs the position on the horizontal plane for each person detected
    by the cameras.

    OSC message is as follows, for each person and body part:
    {base OSC path}/position/{camera_id}/{Person ID}/{world X position} {world Y position} {world Z position}
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(PositionFilter, self).__init__("position", **kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()
        self._parser.add_argument("--Kmats", type=Dict[str, List[float]], default={}, help="Kmats")  # type: ignore
        self._parser.add_argument("--Rtmats", type=Dict[str, List[float]], default={}, help="Rtmats")  # type: ignore
        self._parser.add_argument("--use-pyrealsense2", type=bool, default=False, help="Use pyrealsense2 backend")

    def init(self) -> None:
        if self._args.use_pyrealsense2 and not has_pyrealsense2:
            logger.error(
                "pyrealsense2 library not found. Please install it or set use_pyrealsense2 to False in your config file.")
            sys.exit(1)

        self._Kmatrices: Dict[str, np.array] = {}
        self._Rtmatrices: Dict[str, np.array] = {}  # [R|t]
        # reshape the intrinsics parameters to 3x3 matrices
        for kmat in self._args.Kmats:
            self._Kmatrices[kmat] = np.array([self._args.Kmats[kmat]]).reshape((3, 3))

        # reshape the extrinsics parameters to 3x4 matrices
        for Rtmat in self._args.Rtmats:
            self._Rtmatrices[Rtmat] = np.array([self._args.Rtmats[Rtmat]]).reshape((3, 4))

        # the homography between a world plane at z=0 and the image is H = K[r1, r2 |t]
        # r_i are the columns of rotation matrix R. (Hartley and Zisserman, eqn 8.1)
        self._Hs: Dict[str, np.array] = {}

        for cam, Kmat in self._Kmatrices.items():
            self._Hs[cam] = Kmat @ np.hstack((self._Rtmatrices[cam][:, 0:2],
                                              self._Rtmatrices[cam][:, 3:]))  # K * [r1, r2 |t]

    def compute_position(self, cam_id: int, depth_channels: List[Channel], pt: np.array) -> np.array:
        """
        compute the depth from screen points. Do NOT account for distorsion at the moment.
        :param depth_channels: List[Channel] - List of input depth channels
        """
        if has_pyrealsense2 and self._args.use_pyrealsense2 and len(depth_channels) > 0:
            depth_frame: rs.depth_frame = depth_channels[0].metadata[PyRealSense2Camera.DEPTH_METADATA]
            intrinsics = depth_frame.profile.as_video_stream_profile().get_intrinsics()
            ptr = [int(round(pt[0])), int(round(pt[1]))]
            distance = depth_frame.as_depth_frame().get_distance(ptr[0], ptr[1])
            projected_pt = rs.rs2_deproject_pixel_to_point(intrinsics, ptr, distance)
            return np.array(projected_pt).reshape(3, 1)
        else:
            cam_path = list(self._Kmatrices.keys())[cam_id]
            projected_pt = np.linalg.inv(self._Hs[cam_path]) @ np.r_[pt, 1.0].T
            # normalize by z.
            projected_pt = projected_pt / projected_pt[2]
            return projected_pt.reshape(3, 1)

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        threshold = 0.5
        minimum = 0.3

        # Get input streams and get image frames from them
        depth_channels: List[Channel] = []
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)
        for stream in input_streams.values():
            if stream.has_channel_type(Channel.Type.DEPTH):
                depth_channels.append(stream.get_channels_by_type(Channel.Type.DEPTH)[0])

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        # All 2D poses from all streams are sent
        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                for cam_id, poses_for_camera in enumerate(poses_for_cameras):
                    result[cam_id] = {}

                    for pose_index, pose in enumerate(poses_for_camera.poses):
                        keypoint_dict = pose.keypoints
                        if not keypoint_dict:
                            continue

                        position = np.zeros((3,))

                        # Check if feet were detected with high confidence.
                        left_heel = keypoint_dict.get("LEFT_HEEL")
                        right_heel = keypoint_dict.get("RIGHT_HEEL")

                        if (left_heel is not None and left_heel.confidence > threshold and right_heel is not None and right_heel.confidence > threshold):
                            mid_feet = (np.array(left_heel.position) + np.array(right_heel.position)) / 2.0

                            position = self.compute_position(cam_id, depth_channels, mid_feet)

                        # Else, try using the other feet keypoints, if present.
                        else:
                            toe_keypoints = [
                                keypoint_dict.get("LEFT_BIG_TOE"),
                                keypoint_dict.get("LEFT_SMALL_TOE"),
                                keypoint_dict.get("RIGHT_BIG_TOE"),
                                keypoint_dict.get("RIGHT_SMALL_TOE"),
                                keypoint_dict.get("LEFT_ANKLE"),
                                keypoint_dict.get("RIGHT_ANKLE")
                            ]

                            valid_toe_keypoints = np.array(
                                [[k.position[0], k.position[1], k.confidence] for k in toe_keypoints
                                 if k is not None and k.confidence > threshold]
                            )

                            if (len(valid_toe_keypoints) > 0 and np.max(valid_toe_keypoints[:, 2]) > minimum):
                                # Take most confident foot keypoint.
                                toe = valid_toe_keypoints[np.argmax(valid_toe_keypoints)][0:2]
                                position = self.compute_position(cam_id, depth_channels, toe)

                        result[cam_id][pose_index] = [
                            float(position[0]),
                            float(position[1]),
                            float(position[2])
                        ]

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
