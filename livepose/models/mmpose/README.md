Config files from the [mmpose git submodule](https://gitlab.com/sat-mtl/tools/forks/mmpose) are available in development mode from these paths:
* external/mmpose/configs/
* external/mmpose/demo/mm*_cfg

Model files should be downloaded here with the same subpaths as URLs but withour prefix, for example:
* file ssd/ssdlite_mobilenetv2_scratch_600e_coco/ssdlite_mobilenetv2_scratch_600e_coco_20210629_110627-974d9307.pth is downloaded from https://download.openmmlab.com/mmdetection/v2.0/ssd/ssdlite_mobilenetv2_scratch_600e_coco/ssdlite_mobilenetv2_scratch_600e_coco_20210629_110627-974d9307.pth without https://download.openmmlab.com/mmdetection/v2.0 prefix

When LivePose is packaged, these files are copied under this mmpose models directory. 
