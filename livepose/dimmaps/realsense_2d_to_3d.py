import numpy as np

from typing import Any, Dict, List, Optional, Tuple

from livepose.cameras.pyrealsense2 import PyRealSense2Camera
from livepose.dataflow import Channel, Flow, Stream
from livepose.dimmap import register_dimmap, DimMap
from livepose.pose_backend import PosesForCamera, Pose, Keypoint

import logging

logger = logging.getLogger(__name__)


try:
    import pyrealsense2 as rs
    has_pyrealsense2 = True
except (ImportError, ModuleNotFoundError):
    has_pyrealsense2 = False


@register_dimmap("realsense_2d_to_3d")
class Realsense2DTo3D(DimMap):
    """
    Dimension mapping which uses the depth map from Realsense cameras to convert 2D poses
    to 3D, in the reference frame of the camera. All units end up being in meters.
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(Realsense2DTo3D, self).__init__("realsense_2d_to_3d", **kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()
        self._parser.add_argument("--calibration-matrices", type=List[np.array], default=[],
                                  help="Calibration matrices")  # type: ignore

    def init(self) -> None:
        pass

    def compute_position(self, cam_id: int, color_frames: List[Channel], depth_frames: List[Channel], pt: np.array) -> Optional[Tuple[float, float, float]]:
        """
        Compute the depth from screen points.
        :param frames: List[Dict[str, Any]] - List of all input video frames
        """
        color_frame = color_frames[cam_id].metadata[PyRealSense2Camera.COLOR_METADATA]
        depth_frame = depth_frames[cam_id].metadata[PyRealSense2Camera.DEPTH_METADATA]

        # Apply flipping and rotation to the input points, as the PyRealSense2 frames have
        # not been transformed (they are still the raw channel values)
        if color_frames[cam_id].metadata["flip"]:
            pt[0] = color_frames[cam_id].metadata["resolution"][0] - pt[0]

        if color_frames[cam_id].metadata["rotate"] != 0:
            angle = color_frames[cam_id].metadata["rotate"]
            res_x = color_frames[cam_id].metadata["resolution"][0]
            res_y = color_frames[cam_id].metadata["resolution"][1]

            if angle == 90:
                pt = np.array([
                    res_x - (pt[1] - res_y / 2),
                    (pt[0] - res_x / 2) + res_y
                ])
            elif angle == 180:
                pt = np.array([
                    res_x - (pt[0] - res_x / 2),
                    res_y - (pt[1] - res_y / 2)
                ])
            elif angle == 270:
                pt = np.array([
                    (pt[1] - res_y / 2) - res_x,
                    res_y - (pt[0] - res_x / 2)
                ])

        depth_scale = depth_frames[cam_id].metadata[PyRealSense2Camera.DEPTH_SCALE_METADATA]
        depth_min = 0.1
        depth_max = 2.0
        color_intrinsics = color_frame.profile.as_video_stream_profile().get_intrinsics()
        depth_intrinsics = depth_frame.profile.as_video_stream_profile().get_intrinsics()
        depth_to_color_extrinsics = depth_frame.profile.as_video_stream_profile().get_extrinsics_to(color_frame.profile)
        color_to_depth_extrinsics = color_frame.profile.as_video_stream_profile().get_extrinsics_to(depth_frame.profile)

        depth_ptr = rs.rs2_project_color_pixel_to_depth_pixel(
            depth_frame.get_data(),
            depth_scale,
            depth_min,
            depth_max,
            depth_intrinsics,
            color_intrinsics,
            depth_to_color_extrinsics,
            color_to_depth_extrinsics,
            [pt[0], pt[1]]
        )
        ptr = [int(round(depth_ptr[0])), int(round(depth_ptr[1]))]

        if ptr[0] < 0 or ptr[0] > depth_frame.as_depth_frame().get_width():
            return None

        if ptr[1] < 0 or ptr[1] > depth_frame.as_depth_frame().get_height():
            return None

        distance = depth_frame.as_depth_frame().get_distance(ptr[0], ptr[1])

        if distance == 0.0:
            return None

        projected_pt = rs.rs2_deproject_pixel_to_point(depth_intrinsics, ptr, distance)
        return (projected_pt[0], projected_pt[1], projected_pt[2])

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        poses_3d_by_camera: List[PosesForCamera] = []

        # Get input frames
        color_frames: List[Channel] = []
        depth_frames: List[Channel] = []
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)
        for stream in input_streams.values():
            if stream.has_channel_type(Channel.Type.COLOR):
                color_frames.append(stream.get_channels_by_type(Channel.Type.COLOR)[0])
            if stream.has_channel_type(Channel.Type.DEPTH):
                depth_frames.append(stream.get_channels_by_type(Channel.Type.DEPTH)[0])

        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                for cam_id, poses_for_camera in enumerate(poses_for_cameras):
                    poses_3d: List[Pose] = []
                    for pose in poses_for_camera.poses:
                        keypoints_3d: Dict[str, Keypoint] = {}

                        for name, keypoint in pose.keypoints.items():
                            keypoint_3d = self.compute_position(
                                cam_id, color_frames, depth_frames, np.r_[keypoint.position])

                            if keypoint_3d is None:
                                continue

                            if cam_id < len(self._args.calibration_matrices):
                                keypoint_3d = tuple(
                                    np.dot(np.r_[keypoint_3d, 1.0], np.r_[
                                           self._args.calibration_matrices[cam_id]])  # type: ignore
                                )[0:3]

                            keypoints_3d[name] = Keypoint(
                                confidence=keypoint.confidence,
                                part=name,
                                position=list(keypoint_3d)
                            )

                        pose_3d: Pose = Pose(
                            confidence=pose.confidence,
                            keypoints=keypoints_3d,
                            id=pose.id,
                            keypoints_definitions=pose.keypoints_definitions
                        )

                        poses_3d.append(pose_3d)

                    poses_3d_by_camera.append(PosesForCamera(poses=poses_3d))

        if not flow.has_stream(self.dimmap_name):
            flow.add_stream(name=self.dimmap_name, type=Stream.Type.DIMMAP)

        flow.set_stream_frame(
            name=self.dimmap_name,
            frame=[Channel(
                type=Channel.Type.POSE_3D,
                data=poses_3d_by_camera,
                name=self.dimmap_name,
                metadata={}
            )]
        )
