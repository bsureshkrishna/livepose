import cv2
import numpy as np

from typing import Any, Dict, List, Optional, Tuple
from math import pow, sqrt
from numpy.typing import ArrayLike

from livepose.dataflow import Channel, Flow, Stream
from livepose.dimmap import register_dimmap, DimMap
from livepose.pose_backend import PosesForCamera


@register_dimmap("pose2d_to_floor")
class Pose2DToFloor(DimMap):
    """
    Dimension mapping which outputs the position on an arbitrary plane for each person detected
    by the cameras. The computation is based on a perspective correction which uses
    4 pairs of (3D point, 2D point).

    OSC message is as follows, for each person:
    {base OSC path}/position2D/{camera_id}/{Person ID}/{world X position} {world Y position}
    """

    def __init__(self,
                 *args: Any, **kwargs: Any):
        super(Pose2DToFloor, self).__init__("pose2d_to_floor", **kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()
        self._parser.add_argument("--pairs-per-camera", type=Dict[str, List[Tuple[Tuple[float, float, float], Tuple[float, float, float]]]], default={},  # type: ignore
                                  help="Pairs per camera")
        self._parser.add_argument("--merge-distance", type=float, default=1.0,
                                  help="Merge distance, in the unit decided in the configuration file")
        self._parser.add_argument("--threshold", type=float, default=0.2,
                                  help="Confidence threshold for keypoints to be considered valid")
        self._parser.add_argument("--keypoints", type=List[str], default={"LEFT_ANKLE", "RIGHT_ANKLE"},
                                  help="List of keypoints to track and map on the 2D surface")

    def init(self) -> None:
        self._perspective_transforms: Dict[str, ArrayLike] = {}
        for camera, pairs in self._args.pairs_per_camera.items():
            self._perspective_transforms[camera] = self.compute_perspective_transform(
                pairs)

    def compute_perspective_transform(self, pairs: List[Tuple[Tuple[float, float, float], Tuple[float, float, float]]]) -> ArrayLike:
        assert(len(pairs) == 4)

        src = np.zeros((4, 2), dtype="float32")
        dst = np.zeros((4, 2), dtype="float32")

        for index, pair in enumerate(pairs):
            src[index] = pair[0]
            dst[index] = pair[1]

        perspective_transform = cv2.getPerspectiveTransform(src, dst)
        return perspective_transform

    def compute_position(self, camera: str, coords: Tuple[float, float]) -> Tuple[float, float]:
        assert(camera in self._perspective_transforms)

        perspective_transform = self._perspective_transforms[camera]
        transformed_coords = cv2.perspectiveTransform(
            np.array([[[coords[0], coords[1]]]], dtype="float32"), perspective_transform)
        return (transformed_coords[0][0][0], transformed_coords[0][0][1])

    def merge_positions(self, positions_per_camera: Dict[int, List[Tuple[float, float]]]) -> List[Tuple[float, float]]:
        """
        Merge positions if visible from more than one camera, based on the distance between them

        :param positions_per_camera: Dict[int, List[Tuple[float, float]]] - All detected positions for each camera ID
        :return: List[Tuple[float, float]] - Return the merged positions, not based on camera IDs anymore
        """
        grouped_positions: List[Tuple[Tuple[float, float], float]] = [
        ]  # List of positions and the number of detection for each across cameras
        for positions in positions_per_camera.values():
            new_positions: List[Tuple[float, float]] = []
            for position in positions:
                assert(position is not None)

                # If no position was previously recorded, start now
                if not grouped_positions:
                    new_positions.append(position)

                # Merge with the first close enough from pre-grouped positions
                for index, (existing_position, count) in enumerate(grouped_positions):
                    if sqrt(pow(existing_position[0] - position[0], 2.0) + pow(existing_position[1] - position[1], 2.0)) <= self._args.merge_distance:
                        grouped_positions[index] = (
                            (
                                (existing_position[0] * count +
                                 position[0]) / (count + 1.0),
                                (existing_position[1] * count +
                                 position[1]) / (count + 1.0)
                            ),
                            count + 1.0
                        )
                    else:
                        new_positions.append(position)

            for new_position in new_positions:
                grouped_positions.append((new_position, 1.0))

        return [position for position, count in grouped_positions]

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        threshold = self._args.threshold

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        # All 2D poses from all streams are sent
        for stream in pose_streams.values():
            assert(stream is not None)

            positions_per_camera: Dict[int, List[Tuple[float, float]]] = {}
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                #
                # Evaluate the persons' positions on the floor
                #
                for cam_id, poses_for_camera in enumerate(poses_for_cameras or []):
                    camera_path = list(
                        self._perspective_transforms.keys())[cam_id]
                    for pose_index, pose in enumerate(poses_for_camera.poses):
                        keypoint_dict = pose.keypoints

                        keypoints_list = self._args.keypoints
                        position: Optional[Tuple[float, float]] = None

                        toe_keypoints: List[np.array] = []
                        for keypoint_name in keypoints_list:
                            toe_keypoints.append(keypoint_dict.get(keypoint_name))

                        toe_keypoints_filtered = np.array(
                            [[*k.position, k.confidence] for k in toe_keypoints
                             if k is not None and k.confidence > threshold]
                        )

                        if (len(toe_keypoints_filtered) > 0 and np.max(toe_keypoints_filtered[:, 2]) > threshold):
                            # Take most confident foot keypoint.
                            toe = toe_keypoints_filtered[np.argmax(
                                toe_keypoints_filtered[:, 2])]
                            position = self.compute_position(
                                camera_path, toe[0:2])

                        if position is not None:
                            if cam_id not in positions_per_camera:
                                positions_per_camera[cam_id] = []
                            positions_per_camera[cam_id].append(position)

            grouped_positions = self.merge_positions(positions_per_camera)

            #
            # Prepare data for sending
            #
            for pose_index, position in enumerate(grouped_positions):
                self._result[pose_index] = (
                    float(position[0]),
                    float(position[1])
                )

        if not flow.has_stream(self._dimmap_name):
            flow.add_stream(name=self.dimmap_name, type=Stream.Type.DIMMAP)

        flow.set_stream_frame(
            name=self._dimmap_name,
            frame=[Channel(
                type=Channel.Type.POSE_FLOOR,
                data=self._result,
                name=self._dimmap_name,
                metadata={}
            )]
        )
