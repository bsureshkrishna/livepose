from livepose.dataflow import Channel
from livepose.camera import register_camera, Camera
from rosbags.rosbag1 import Reader
from rosbags.serde import deserialize_cdr, ros1_to_cdr
from typing import Any, List, Optional, Tuple

import logging
import numpy
import os
import time

logger = logging.getLogger(__name__)


@register_camera("rosbag")
class RosbagCamera(Camera):
    """
    Utility class embedding a camera from a rosbag file
    """

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self._list_files: List[str] = []
        self._current_file_id: int = 0
        self._reader: Optional[Reader] = None
        self._last_message: Any = None
        self._reader_messages: Any = None
        self._height = 480
        self._width = 640
        self._fps = 1.0

        # For frame synchronization
        self._start_time = 0
        self._next_message_timestamp = 0

    def add_parameters(self) -> None:
        super().add_parameters()

    def _setup_reader_and_times(self) -> None:
        """
        Sets up the rosbag file reader and the time variables necessary for synchronization with timestamps
        Needs to be run every time a new rosbag file is opened or looped
        """
        self._reader = Reader(self._list_files[self._current_file_id])
        self._reader.open()
        logger.info(
            f"Opening rosbag file {self._list_files[self._current_file_id]}")
        self._reader_messages = self._reader.messages()
        serialized_message = next(self._reader_messages)
        self._last_message = deserialize_cdr(ros1_to_cdr(
            serialized_message[2], serialized_message[0].msgtype), serialized_message[0].msgtype)
        current_frame_time = self._last_message.header.stamp.sec * pow(10, 9) + self._last_message.header.stamp.nanosec
        self._start_time = time.time_ns() - current_frame_time
        self._next_message_timestamp = current_frame_time

    def init(self) -> None:
        # Gather list of rosbag files to read
        count_star = self._args.path.count("*")
        if count_star == 1:
            r_partition_path = self._args.path.rpartition("/")
            if "*" in r_partition_path[2]:
                dir_path = r_partition_path[0]
                for file_name in os.listdir(dir_path):
                    r_partition_file_name = r_partition_path[2].rpartition("*")
                    if r_partition_file_name[0] in file_name and r_partition_file_name[2] in file_name:
                        self._list_files.append(
                            r_partition_path[0] + "/" + file_name)
            else:
                logger.error(f"The camera path {self._args.path} is invalid")
        elif count_star == 0:
            self._list_files.append(self._args.path)
        else:
            logger.error(f"The camera path {self._args.path} is invalid")
        self._list_files.sort(key=str.lower)

        if len(self._list_files) == 0:
            logger.error(f"No file corresponds to {self._args.path}")

        self._setup_reader_and_times()

    def close(self) -> None:
        if self._reader is not None:
            self._reader.close()
            self._reader = None

    @property
    def fps(self) -> float:
        return self._fps

    @fps.setter
    def fps(self, framerate: float) -> None:
        # Doesn't really make sense for this type of camera
        return None

    def get_raw_channels(self) -> List[Channel]:
        """
        Get the raw channels from the camera, without any
        transformation (flip, rotate, etc) applied
        :return: List[Channel] - List of raw channels
        """
        # Sleep to adjust to frametime
        time_diff = self._next_message_timestamp - \
            (time.time_ns() - self._start_time)
        if time_diff > 0:
            time.sleep(time_diff / pow(10, 9))

        channels: List[Channel] = []

        current_frame = True
        while (current_frame):
            # Camera data
            if self._last_message.__msgtype__ == "sensor_msgs/msg/Image":
                # Update height/width
                if self._height != self._last_message.height:
                    self._height = self._last_message.height
                if self._width != self._last_message.width:
                    self._width == self._last_message.width
                # Color image
                if self._last_message.encoding == "bgr8":
                    data_array = self._last_message.data.reshape(self._last_message.height, self._last_message.width, 3)
                    if data_array is not None:
                        channels.append(Channel(
                            type=Channel.Type.COLOR,
                            name=self._list_files[self._current_file_id],
                            data=data_array.copy(),
                            metadata={}
                        ))
                # Depth
                elif self._last_message.encoding == "mono16":
                    data_array = self._last_message.data.view(numpy.uint16).reshape(
                        self._last_message.height, self._last_message.width, 1)
                    if data_array is not None:
                        channels.append(Channel(
                            type=Channel.Type.DEPTH,
                            name=self._list_files[self._current_file_id],
                            data=data_array.copy(),
                            metadata={}
                        ))
            # Other data (We assume it is a skeleton tracker for now)
            elif self._last_message.__msgtype__ == "std_msgs/msg/String":
                pass
            try:
                serialized_message = next(self._reader_messages)
                self._last_message = deserialize_cdr(ros1_to_cdr(
                    serialized_message[2], serialized_message[0].msgtype), serialized_message[0].msgtype)
            except StopIteration:
                if self._reader is not None:
                    self._reader.close()
                    self._current_file_id = (self._current_file_id + 1) % len(self._list_files)
                    self._setup_reader_and_times()
            try:
                new_time = self._last_message.header.stamp.sec * pow(10, 9) + self._last_message.header.stamp.nanosec
                if new_time > self._next_message_timestamp:
                    self._fps = 1 / ((new_time - self._next_message_timestamp) / pow(10, 9))
                    self._next_message_timestamp = new_time
                    current_frame = False
            except BaseException:
                pass  # If you can't access the timestamp, assume it's a message occuring in the same frame as the previous one
        return channels

    def grab(self) -> bool:
        """
        Grabs the next frame from video file or capturing device
        :return: bool - in the case of success
        """
        return True

    def retrieve(self) -> bool:
        """
        Decodes and set the grabbed video frame
        grab() should be called beforehand
        :return: bool - true in the case of success
        """
        return True

    def is_open(self) -> bool:
        """
        Check if a video capturing device has been initialized already
        :return: bool
        """
        return self._reader is not None

    def open(self) -> bool:
        """
        Open video file or device
        :return: bool - true if successful
        """
        if self._reader is None:
            self._setup_reader_and_times()
        return self._reader is not None

    @property
    def resolution(self) -> Tuple[int, int]:
        """
        Get the resolution of the device
        :return: Tuple[int, int]
        """
        return (self._width, self._height)

    @resolution.setter
    def resolution(self, res: Tuple[int, int]) -> None:
        """
        Set the resolution of the device
        :param: Tuple[int, int]
        """
        pass
