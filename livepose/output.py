import argparse

from abc import ABCMeta, abstractmethod

from typing import Any, Callable, Dict, Optional, Set, Type

from livepose.dataflow import Flow
from livepose.filter import Filter
from livepose.node import Node


def register_output(name: str) -> Callable:
    """
    Decorator  for registering outputs
    :param name: name - Name of the output
    """

    def deco(cls: Type['Output']) -> Callable:
        Output.register(output_type=cls, name=name)
        return cls

    return deco


class Output(Node):
    """
    Base class for outputs
    An output gets to send the results from the various filters
    """

    registered_outputs: Dict[str, Type['Output']] = dict()

    def __init__(self, output_name: str, *args: Any, **kwargs: Any):
        """Init Output class."""
        super().__init__(**kwargs)
        self._output_name: str = output_name

    @abstractmethod
    def add_parameters(self) -> None:
        """Add Output parameters."""
        super().add_parameters()

    @abstractmethod
    def init(self) -> None:
        """Init Output objects."""
        pass

    @classmethod
    def register(cls, output_type: Type['Output'], name: str) -> None:
        if name not in cls.registered_outputs:
            cls.registered_outputs[name] = output_type
        else:
            raise Exception(f"An output type {name} has already been registered")

    @classmethod
    def create_from_name(cls, name: str, **kwargs: Any) -> Optional['Output']:
        if name not in Output.registered_outputs:
            return None
        else:
            return Output.registered_outputs[name](**kwargs)

    def send_flow(self, flow: Flow, now: float, dt: float) -> None:
        """
        Send the results for the given filter
        :param flow: Flow which outputs will be sent
        """
        pass

    def stop(self) -> None:
        """
        Stop output
        """
        pass
