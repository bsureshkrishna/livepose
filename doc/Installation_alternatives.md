Alternative Installations
============
This page contains a collection of instructions for customizing the
installation of LivePose and its dependencies.

Note that Ubuntu 22.04 and 20.04 are the only versions of Ubuntu supported for LivePose.
There are no installation instructions for older Ubuntu versions.

The alternative installations each have their own page:
- [Installing Without GPU Support](doc/Installation_no_gpu.md)
- [Compiling Dependencies From Source](doc/Installation_compile_from_source.md)
- [Installing on a Jetson board](doc/Installation_jetson.md)
