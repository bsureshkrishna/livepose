Installation
============
Steps for installing LivePose and all of its dependencies on Ubuntu systems.

**Note:** These instructions are only meant for installing on **Ubuntu 22.04 or 20.04**
and assume you have an Nvidia GPU. If you are using LivePose on a Jetson board,
do not have a GPU, or would like to otherwise customize your installation
please use our
[alternative installation instructions](doc/Installation_alternatives.md)

## Clone The LivePose Repo

1. Clone the LivePose repo
```bash
sudo apt install git
git clone https://gitlab.com/sat-mtl/tools/livepose
cd livepose
```

2. Install and setup Git LFS to pull models
```bash
sudo apt install git-lfs
git lfs fetch && git lfs pull
```
## Install Dependencies
### Install Main Dependencies with apt

* Install dependencies for setting up apt repositories:
```bash
sudo apt install coreutils gnupg2 wget sed software-properties-common
```
- Setup NVIDIA repository (for CUDA, CUDNN, TensorRT):
```bash
OS="ubuntu`lsb_release -rs | sed 's/\.//'`" && \
wget https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/cuda-${OS}.pin && \
sudo mv cuda-${OS}.pin /etc/apt/preferences.d/cuda-repository-pin-600 && \
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/3bf863cc.pub && \
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/ /"
```
- Setup sat-metalab MPA (for CUDA-enabled dependencies for backends using OpenCV, TensorRT): 
```bash
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-$(lsb_release -cs)-amd64-nvidia/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo "deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-$(lsb_release -cs)-amd64-nvidia/debs/ sat-metalab main" \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa.list
```
- Setup sat-metalab datasets MPA (models for LivePose pose detection backends): 
```bash
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-datasets/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list
```
- Install main dependencies:
```bash
sudo apt update && \
sudo apt install \
    build-essential libpython3-dev python3 python3-pip python3-setuptools python3-venv python3-wheel \
    python3-cycler python3-dateutil python3-dev python3-filterpy python3-getch python3-imageio python3-lap \
    python3-matplotlib python3-networkx python3-numpy python3-scipy python3-six python3-skimage python3-tk \
    v4l-utils \
    python3-mediapipe \
    python3-mmcv python3-mmdetection python3-xtcocotools python3-mmpose \
    python3-opencv \
    python3-liblo \
    python3-libmapper \
    python3-librealsense2 \
    python3-torch python3-torchvision python3-torch2trt python3-trt-pose \
    python3-websockets

```

### (Optional) Install Intel RealSense camera support

This step is only needed if you are using Intel RealSense cameras.

- Install the libraries:
```
sudo apt-get install librealsense2-udev-rules python3-librealsense2
```

The above line will deploy librealsense2 udev rules, and install the pyrealsense2 python API.

Notes from LivePose developers: 
- At the time of writing, `librealsense2-dkms` fails to rebuild dkms module for linux kernel 5.13 (current version on Ubuntu 20.04) so features accessible through `librealsense` might be limited. 
- At least `librealsense2-udev-rules` is required for `[pyrealsense2](https://pypi.org/project/pyrealsense2/)` to access the camera. Otherwise, by running `python3 tools/test_pyrealsense2.py` you will get an error similar to this:
```
Traceback (most recent call last):
  File "livepose/tests/test_pyrealsense2.py", line 3, in <module>
    profile = pipe.start()
RuntimeError: Failed to open scan_element /sys/devices/pci0000:00/0000:00:14.0/usb2/2-2/2-2:1.5/0003:8086:0B5C.0004/HID-SENSOR-200076.3.auto/iio:device1/scan_elements/in_anglvel_x_en Last Error: Permission denied
```

On gnome desktop environments, the desktop orientation gets automatically bound to the orientation of the RealSense camera. To disable this feature: 
```
gsettings set org.gnome.settings-daemon.peripherals.touchscreen orientation-lock true
```
### (If you have NVIDIA hardware) Ensure that you are running the latest drivers

Copy and paste this command in a terminal:
```
sudo apt install nvidia-driver-`apt search --names-only nvidia\-driver | awk -F '/' '{print $1}' | awk -F '-' '{print $3}' | sort -ur | head -n1`
```

Watch for the amount of `newly installed` packages the last line output by this command. If your drivers have been updated, you will need to reboot for the new drivers to take effect.

### Create a Virtual Environment

LivePose has Python dependencies installed through apt and pip. 
We create a virtual environment so that LivePose can access dependencies from both sources. 

1. Create a virtual environment (you can name this whatever you like)
```bash
python3 -m venv --system-site-packages --symlinks ~/livepose_venv
```

2. Activate the virtual environment
```bash
source ~/livepose_venv/bin/activate
```

3. Install remaining Python dependencies with pip
```bash
export SETUPTOOLS_USE_DISTUTILS=stdlib # ModuleNotFoundError: No module named 'setuptools._distutils'
pip3 install .
```

#### Optional extras

Optional components (cameras, backends, filters and outputs) can be activated as a semicolon-separated list with environment variable `LIVEPOSE_EXTRAS`. 
Examples:
- `pip3 install .`: if `LIVEPOSE_EXTRAS` is not defined, by default all extras are activated their dependencies searched and required
- `LIVEPOSE_EXTRAS="all" pip3 install .`: activates all extras and installs their dependencies (useful if `LIVEPOSE_EXTRAS` had already been set to other values)
- `LIVEPOSE_EXTRAS="posenet;osc" pip3 install .`: activates and searches dependencies for `posenet` and `osc`, but deactivates all other extras (such as `libmapper`, ...)

Tip: use option `-e` in development mode, so that `pip3 install -e .` installs all dependencies but LivePose itself, and thus LivePose source code modifications are active directly at runtime without re-installation.

Note for developers: this command is required to be run everytime after switching between branches that provide different components.

## Run the Demo
You can now run LivePose!
To try it out with the default settings run the following command from the top-
level LivePose directory: 
```bash
./livepose.sh
```

To exit the virtual environment when you are not installing or using
LivePose:
```bash
deactivate
```
