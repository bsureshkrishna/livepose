OSC
===

LivePose uses Open Sound Control (OSC) messages for outputting pose and action
data in real-time.

If you are not familiar with OSC, you can read more about it on the official
[website](http://opensoundcontrol.org/).

This document contains specifications for all OSC messages used by LivePose.
For example code, see the [Example Server](#example-server) section.

## OSC Message Format
OSC Messages are sent out on a specific address using a special **path**.
The path specifies what type of message is being sent and how it should be
handled.
A different message path must be reserved for every type of message.

LivePose uses different message paths for each type of filter and their outputs.
We also use a few messages to mark the start and end of each video
frame and the start and end of each filter output.

All message paths are listed below, grouped by filter.


## OSC Path Cheatsheet
Listed here is every OSC message path used by LivePose. The first section is
for general paths, used everytime LivePose is run. Each other section is named
for a specific filter and outlines the message paths used to send that filter's
output.

**NOTE**: `/livepose` is the default base path for all LivePose OSC messages.
All message paths must begin with `/livepose`, followed by a distinguishing
sub-path.


### General Paths
These paths are used every-time LivePose is run and are not filter-specific.
Each of the filters have their own paths and output specs, which are outlined
in the next sections.

| Message Path                           | Args        | Description |
| -------------------------------------- | ----------- | ----------- |
| `/livepose/frame`                      | `now`, `dt` | Marks the start of a video frame. All messages that follow pertain to this specific frame, until the `/end_frame` message is received. `now`, and `dt` are `float` marking the time (in seconds) since livepose was started and the time since the last frame, respectively. |
| `/livepose/end_frame`                  | `now`, `dt` | Marks the end of the current video frame. No other messages will be received until a `/frame` message marks the start of the next frame. `now` and `dt` are same as for `/livepose/frame`. |
| `/livepose/{filter_name}/start_filter` |  none       | Marks the start of the filter specified by `{filter_name}`. All the messages that follow pertain to this filter, until the `{filter_name}/end_filter` message is received. |
| `/livepose/{filter_name}/end_filter`   |  none       | Marks the end of the current filter. No other messages will be received until the next filter is started. |


### Filter Message Paths
Below is a section for each filter with the filters message paths.
Note that each filter uses different sub-paths for each camera.
Each camera has a unique index specified by `{cam_id}`.
i.e. all messages from the first camera will use base path
`/livepose/{filter_name}/0`

Many filters also use sub-paths to specify different detected persons.
`{pose_index}` is a unique index used to specify which detected person the
output corresponds to. `{pose_index}` is the same across all filters, but
may not be the same between frames! For tracking people between frames, use
the `pose_id` attribute output by the
[skeletons filter](#skeletons-filter)

i.e. In any given frame `pose_index` `0` in the skeletons filter is the same as
`pose_index` `0` in the armup filter, for example.


### Skeletons Filter
The skeletons filter sends out a single message for each keypoint detected.
The keypoint is specified by `{keypoint_name}` (e.g. `'NOSE'`,
`'LEFT_SHOULDER'`, etc.).
The full list of keypoint names can be found in
[livepose/keypoint.py](livepose.keypoint.py)

`{pose_index}` is used to index the poses across all filters; note, however,
that the indices may not be the same from frame to frame!

`{pose_id}` should be used for tracking poses from frame to frame. It is given
in its own OSC message shown in the table below. The message will either contain
an `int` of the unique pose ID tracked across all frames, or `None` if no pose
ID was found for the given pose.

The message for each keypoint contains the image coordinates where the
keypoint was detected, along with the detection confidence score.


| Message Path                                                          | Args                 | Description |
| --------------------------------------------------------------------- | -------------------- | ----------- |
| `/livepose/skeletons/{cam_id}/{pose_index}/pose_id`                   | `pose_id`            | Returns the unique ID of the current pose, as tracked across all frames. May be `None` if a tracker ID could not be found for the pose with high confidence. |
| `/livepose/skeletons/{cam_id}/{pose_index}/keypoints/{keypoint_name}` | `(x, y, confidence)` | Returns the image coordinates of the keypoint `{keypoint_name}` and detection confidence score (between `0.0` and `1.0`) for the person referenced by `{person_id}`. |


### Armup Filter
The armup filter sends out a message for each arm it detects being raised.
The camera id, person id, and left vs right arm are all specified by the
message path.

`{pose_index}` is the index of the person in the list of detected poses
returned by the skeletons filter.

| Message Path                                 | Args | Description |
| -------------------------------------------- | ---- | ----------- |
| `/livepose/armup/{cam_id}/{pose_index}/left`  | none | Indicates left arm of `{pose_index}` raised. |
| `/livepose/armup/{cam_id}/{pose_index}/right` | none | Indicates right arm of `{pose_index}` raised. |


### Number of IDs Filter
The Number of IDs filter sends out a single message with the number of uniquely
detected persons in the frame.

| Message Path                  | Args     | Description |
| ----------------------------- | -------- | ----------- |
| `/livepose/numberID/{cam_id}` | `num_ids`| Returns the number of unique people detected in the specified camera. |


### Orientation Filter
The Orientation Filter returns an orientation in degrees with respect to the
camera. An orientation of zero degrees represents a person facing directly
towards or away from the camera. An orientation of 90 degrees represents a
person standing perpendicular to the camera. The filter also returns the limb
lenghts used to calculate the orientation.

`{pose_index}` is the index of the person in the list of detected skeletons
returned by the skeletons filter.

| Message Path                                  | Args | Description |
| --------------------------------------------- | ---- | ----------- |
| `/livepose/orientation/{cam_id}/{pose_index}` | `(orientation, shoulders, torso, right_arm, left_arm, right_thigh, left_thigh)`| Returns orientation (in degrees) and limb lengths for the specified person and camera. |

## Example Server
We have included an example script,
[tools/osc_server.py](tools/osc_server.py),
for a minimal OSC Server that can be run alongside LivePose.

The script only out the results of the `skeletons` filter for each frame.
Handler methods are included for the basic OSC messages, such as `/frame` and
`/end_frame`.
Handler methods for other messages filters could be added using the included
code as a starting point.

### Usage
The server script must be run in a separate terminal window, before starting
LivePose. Skeleton filter results will then be printed for each frame.

```bash
# In one terminal window.
python3 tools/osc_server.py

# In a separate terminal window.
./livepose.sh
```
