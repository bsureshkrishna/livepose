[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![pipeline status](https://gitlab.com/sat-mtl/tools/livepose/badges/develop/pipeline.svg)](https://gitlab.com/sat-mtl/tools/livepose/commits/develop)
[![coverage report](https://gitlab.com/sat-mtl/tools/livepose/badges/develop/coverage.svg)](https://gitlab.com/sat-mtl/tools/livepose/commits/develop)

# Introduction

LivePose is a command line tool which tracks people skeletons from a RGB or grayscale video feed (live or not), applies various filters on them (for detection, selection, improving the data, etc) and sends the results  through the network (OSC and Websocket are currently supported).

As some example of what the software can do, there are filters to detect specific actions (e.g. raising arms), detecting skeletons position onto a plane, controlling the avatar in [Mozilla Hubs](https://hubs.mozilla.org), etc. There are some ongoing work to add support for 3D skeleton detection using multiple cameras, and for more robust action detection using a neural network.

This software has been tested on Ubuntu 20.04 and 22.04.

# Installation

Read [Installation](./Installation.md) for installation instructions.

# Usage and Configuration

Read [Usage](./Usage.md) to understand how to launch LivePose.

Read [Config](./Config.md) to understand how to create configuration files.

Read [OSC](./OSC.md) to discover all Open Sound Control messages sent by LivePose.

# Contributing and Development

Read [Contributing](./Contributing.md) for development instructions.
