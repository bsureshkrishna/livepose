import argparse
import os

from typing import Any, Dict, List
from rosbags.rosbag1 import Reader, Writer

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--path", type=str, default="./*.bag",
                    help="Path of the files we want to merge. Use * to define the charging part of the name across multiple files.")
params = parser.parse_args()

# Gather list of rosbag files to merge
rosbag_files: List[str] = []
if params.path.count("*") == 1 and params.path.lower().endswith(".bag"):
    r_partition_path = params.path.rpartition("/")
    if "*" in r_partition_path[2]:
        dir_path = r_partition_path[0]
        for file_name in os.listdir(dir_path):
            r_partition_file_name = r_partition_path[2].rpartition("*")
            if r_partition_file_name[0] in file_name and r_partition_file_name[2] in file_name:
                rosbag_files.append(r_partition_path[0] + "/" + file_name)
else:
    print(f"Invalid path: {params.path}")
    exit()

if len(rosbag_files) == 0:
    print("No rosbag files found")
    exit()

rosbag_files.sort(key=str.lower)

new_file_path = params.path.replace("*", "merged")
if os.path.isfile(new_file_path):
    print(f"File: {new_file_path} already exists, either delete it or change its name")
    exit()

connections: Dict[str, Any] = {}

# add new connection
with Writer(new_file_path) as writer:
    for rosbag_file in rosbag_files:
        with Reader(rosbag_file) as reader:
            print("Merging: " + rosbag_file)
            for connection in reader.connections:
                if connection.topic not in connections.keys():
                    connections[connection.topic] = writer.add_connection(connection.topic, connection.msgtype)
            for connection, timestamp, rawdata in reader.messages():
                writer.write(connections[connection.topic], timestamp, rawdata)
