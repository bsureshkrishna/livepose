#!/bin/sh
git log --since="$1" --merges --format="%B %h %cd" --date=short | sed -re "s/Merge branch '([^/-]*).*'.*/* \1/" | sed -z "s/\n\nSee merge request//g" | sed -z "s/\n\n/ /g" | sed -s "s/sat-metalab\/livepose//g" | sed -z -re "s/\n( [a-z0-9]*)/\1/g" | sed -z "s/* [a-z]* [a-z0-9]* [0-9\-]*\n//g" | grep -v "#" | grep -v "remote-tracking"
