Addon Splash
============

This repository contains two scripts, which have to be ran one after the other. The first one is meant to determine the characteristics of the cameras, also known as the intrinsic parameters. The second one is used for estimating their poses (position and orientation) in the physical scene, also know as their extrinsic parameters.


## Calibration of the intrinsic parameters

`chessboard_calibrate.py` is a tool to estimate the parameters of the camera using a checkerboard pattern. It will save the camera parameters to a file. You can find a checkerboard pattern [here](data/pattern_chessboard.png). Take care to print or copy the checkerboard pattern on a perfectly flat surface (a rigid cardboard for example).

Run in the terminal:
```bash
./chessboard_calibrate.py --camera /path/to/camera --square_size 21.0 --pattern_size 10 7
```
While default values are provided, two options are required to have proper results: 
* The option `square_size` corresponds to the size of an individual square of the checkerboard pattern in millimeters. 
* The other parameter is the `pattern_size`, it is the dimensions of the pattern. It accepts two integers corresponding to the number of corners of the chessboard. The first one is the number of columns of the checkerboard pattern, the second integer is the number of rows of the pattern.

To see the available options and their description:
```bash
./chessboard_calibrate.py --help
```

![Intrinsics calibration](data/intrinsics_calibration.png)

Once launched a window will appear, showing the live stream from the camera. The framerate will be low until it detects the pattern. It will get faster once the pattern is detected. Move the camera around while keeping the pattern in the frame until the calibration is completed. If calibration is successful it will create a file name `camera_parameters.json` in the current directory which will look like this:

```json
{
    "proj_error": 0.8124848502163823,
    "camera_matrix": [
        630.1721161286164,
        0.0,
        340.1573821117727,
        0.0,
        634.67835231384,
        254.52840135912254,
        0.0,
        0.0,
        1.0
    ],
    "distortion_coeffs": [
        0.06343236777295372,
        0.4240464565567057,
        0.0,
        0.0,
        -1.6977034092945615
    ],
    "frame_resolution": [
        640,
        480
    ]
}
```

The various sections are:

* `proj_error`: computed reprojection error, in pixels. The lower the better, here it is a bit less than one pixel.
* `camera_matrix`: camera matrix holding the horizontal and vertical fields of view, as well as the optical center position.
* `distortion_coeffs`: distortion coefficients for the camera.
* `frame_resolution`: resolution of the captured frame, as this impacts the computed values.


## Calibration of the extrinsic parameters

`calibrate_camera.py` is an addon for [Splash](https://gitlab.com/sat-mtl/tools/splash) using its calibration tool to compute the extrinsic parameters for all cameras.

To use this script, you must have calibrated the intrinsic parameters of each camera separately as described in the previous section. Then you have to :

* prepare the configuration file
* run the script through Splash
* calibrate each camera

### Configuration file

The addon uses JSON files for configuring various options and settings.
A configuration file with all parameters explicitly set is included in
this repo: tools/addon_splash/config/example.json.

| Parameter Name            | Type / Possible Values              | Description |
|---------------------------|-------------------------------------|---------------|
| `"planar"`                | `"bool"`                            | Whether the calibration surface is planar. For non-planar surfaces, the intrinsics parameters are required |
| `"calib_pts"`             | `List[List[float, float, float]]`   | List of 3D points used to perform calibration. For flat surfaces, the z-coordinates must be zero. A minimum of 6 points is required |
| `"input.paths"`           | `Dict[str, str]`                    | The "input.paths" allows to specify the path to the camera. The "input.paths" parameter is a dictionary of strings, where the key is some nickname of a camera and the value is the path to this camera |
| `"input.intrinsics_paths"`| `Dict[str, str]`                    | Absolute path to the camera intrinsic parameters. The "input.intrinsics_paths" is a dictionary of strings where the key is the camera nickname and the value associated to the key is the path to the intrinsics parameters of the camera. It is optional in the case of planar calibration |

A full configuration file can look like this:

```json
{
    "planar": true,
    "calib_pts" : [
        [0.0, 0.0, 0.0],
        [-1.0, -1.0, 0.0],
        [1.0, 1.0, 0.0],
        [-1.0, 0.0, 0.0],
        [1.0, 0.0, 0.0],
        [0.0, -1.0, 0.0],
        [0.0, 1.0, 0.0]
    ],
    "input.paths": {
        "camera_1": "/dev/video0"
    },
    "input.intrinsics_paths": {
        "camera_1": "camera_parameters.json"
    }
}
```

If more than one camera is used, the `input.paths` and `input.intrinsics_paths` must be updated accordingly. Also the name must match (here, `camera_1` in both lists).

`camera_parameters.json` is the default path output the intrinsics calibrating script (`chessboard_calibrate.py`). Update this path if you renamed the output file of this script.

Note that the coordinates for the 3D points do not have to use a specific unit system. But if you want to get the extrinsics parameters in the metric system, you should specify them in meters. Identify 6-7 points on the desired detection surface and note their 3D coordinates in the configuration file under `"calib_pts"`.

Below is an example of a calibrated floor where a grid was mapped to the floor.

![Calibrated space](data/calibrated_space.png)

### Running the script through Splash

To run the script, you need to have [Splash](https://sat-mtl.gitlab.io/tools/splash) installed. An example with the default example script is as follows:

```bash
splash -P calibrate_camera.py -- config/example.json
```

Where `calibration_config.json` is the configuration file for the addon.

The Splash default GUI should appear, as well as an additional window meant for calibrating the cameras.

### Calibrate the points

A live capture from the camera will be displayed to screen. On the image, double-click on the points corresponding to the 3D points in the same as the `calib_pts` field in the configuration file. This is necessary to have an direct association of the 3D coordinates to the 2D point on the image.

The script will then output the calibration parameters of the camera to file if the reprojection error is small enough. The output file for each camera is named accordingly with the camera names in the `input.paths` and `input.intrinsics_paths` lists of the configuration file. In the case of the default example configuration `config/example.json`, the output file for the only camera will be `camera_1_parameters.json`.

The output file will look as follows:

```json
{
    "proj_error": 0.8415771171028107,
    "camera_matrix": [
        476.99725685934897,
        0.0,
        245.51217805894126,
        0.0,
        927.4368897005767,
        288.16198379168657,
        0.0,
        0.0,
        1.0
    ],
    "distortion_coeffs": [
        -0.6605726391147219,
        3.0167722702207915,
        0.0,
        0.0,
        -5.326167010408955
    ],
    "extrinsics": [
        0.5911437190590846,
        0.8046599062382478,
        -0.05542146434051992,
        1.3116959649518443,
        0.15427985349769374,
        -0.18025072106958268,
        -0.9714460378006724,
        -0.03651644342139517,
        -0.7916734366023175,
        0.5657138082516189,
        -0.23069689406605987,
        5.101086395395033
    ],
    ...
}
```

The `camera_matrix` and `distortion_coeffs` should be close to what was computed in the previous section, you don't have to worry about them. The interesting field is `extrinsics`. It is a 4x3 transformation matrix, which lacks the last line to be a full 4x4 transformation matrix. In the case of this example the full matrix will look like this:

```
[[0.5911437190590846, 0.8046599062382478, -0.05542146434051992, 1.3116959649518443],
 [0.15427985349769374, -0.18025072106958268, -0.9714460378006724, -0.03651644342139517],
 [-0.7916734366023175, 0.5657138082516189, -0.23069689406605987, 5.101086395395033],
 [0.0, 0.0, 0.0, 1.0]]
```

The upper left 3x3 part is the rotation matrix describing the orientation of the camera. The 1x3 part in the upper right is the translation of the camera from the origin of the 3D scene.

### Best practice when using the addon
OpenCV models 2 types of distortion: tangential and radial distortion. Points away from the center of the image exhibit more distortion.
With this in mind, when calibrating, try to use points that occupy the whole image range and make sure that points are not clustered in a section of the image, they should be distributed relatively equally.

Below are two examples of immersive spaces that required camera calibration. The red markers are the chosen calibration points. The blue lines were added to guide the user in its choice of calibration points as we try to select an equal number of points in each quadrant.

![First example](data/calibration.png) ![Second example](data/calibration2.png)

## Recommendations

### Which script to use for the calibration of camera?
The above scripts can both be used to calibrate a camera. The script `chessboard_calibrate.py` is the simplest to use.
Show a checkerboard pattern to the camera at different orientations to estimate the camera parameters and it will saved them to file.

The addon to `Splash` provides a solution to the case where the camera needs to be calibrated in a specific frame of reference.
It supports planar calibration surfaces and non-planar surfaces. For non-planar surfaces, the intrinsic parameters of the camera are required.
In this scenario, we recommand pre-calibrating the camera with the `chessboard_calibrate.py` script.
