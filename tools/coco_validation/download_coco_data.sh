#! /bin/bash

# Downloads images and annotations for the 2017 COCO Validation Dataset and
# places them in the destination directory (./coco/ by default). 

# Two sub-directories will be created in the destination directory:
# `val2017/` and `annotations/`, containing the validation images and their
# groundtruth annotated results, respectively.

set -e

# DEFAULT DESTINATION is a `coco/` sub-directory
# within the current working directory.
DESTINATION="coco"

usage()
{
cat << EOF
Download COCO 2017 Validation Images and Annotations

Usage:
./download_coco_val_2017_data.sh
./download_coco_val_2017_data.sh [-h] [-d DESTINATION]

OPTIONS
-h, --help                       Display this message and exit.

-d, --destination DESTINATION    Download the images and annotations to DESTINATION directory.
                                 If DESTINATION does not exist, it will be created.
                                 If DESTINATION exists but it is not a directory, the script will exit with an error.
                                 (Default: ~/coco/)
EOF
}

while [[ "$#" -gt 0 ]]; do
  case $1 in 
    -h | --help)
      usage
      exit 1
    ;;
    -d | --destination)
      # Check if destination string given
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        DESTINATION=$2
        shift 2
      else
        echo "Error: Missing argument after $1" >&2
        exit 1
      fi
    ;;
    -* )
      echo "Error: Unsupported option $1" >&2
      exit 1
    ;;
    * )
      echo "Error: Unsupported positional argument $1" >&2
      exit 1
      ;;
  esac
done

# Check if destination already exists
if [ -f "$DESTINATION" ]; then
  echo "Error: Destination must be a directory. $DESTINATION exists but is not a directory" >&2
  exit 1 
fi

# Create destinatino directory if it doesn't already exist.
if [ ! -d "$DESTINATION" ]; then
  echo "Creating directory $DESTINATION"
  mkdir $DESTINATION
fi

cd $DESTINATION

# Download dataset.
echo "Downloading images to ${DESTINATION}/val2017/"
wget -O images.zip http://images.cocodataset.org/zips/val2017.zip 
unzip images.zip
rm images.zip

echo "Downloading annotations to ${DESTINATION}/annotations/"
wget -O annotations.zip http://images.cocodataset.org/annotations/annotations_trainval2017.zip
unzip annotations.zip
rm annotations.zip
