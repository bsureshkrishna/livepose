#!/usr/bin/env python3

"""Run inference with LivePose on MOT 20 Train Dataset."""

import argparse
import datetime
import glob
import logging
import os
import random
import sys

from pathlib import Path

import cv2

# Add LivePose to python path.
sys.path.insert(0, os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "../../"
)))

import livepose.pose_backends
from livepose.pose_backend import PoseBackend


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

MOT_20_TRAIN_DIR = os.path.join(
    os.getenv('HOME'),
    "MOT20/train"
)

# Paths of individual video sequence cases within the training set.
MOT_20_SEQUENCE_PATHS = (
   "MOT20-01",
   "MOT20-02",
   "MOT20-03",
   "MOT20-05"
)

# Name of sub-directory containing the image frames of the sequence.
IMAGE_DIR = "img1"

# Top level directory for storing all MOT 20 results for all different backends.
OUTPUT_BASE_DIR = os.path.join(
    os.getenv("HOME"),
    "livepose_mot20_results"
)

# Subdirectory containing results for each video sequence.
OUTPUT_DATA_DIR = "data"

# Deep learning backends which have been configured for tracking.
# ONLY these backends can be used with this script.
AVAILABLE_BACKENDS = ["openpose_wrapper", "openpose_opencv"]


def add_to_results(bounding_boxes, frame_idx, results):
    """Add bounding boxes to list of results in MOT20 format.
    There is one line in results for each bounding box in each frame.
    """
    for bbox in bounding_boxes:
        min_x, min_y, max_x, max_y, _, id = bbox 

        bbox_width = max_x - min_x
        bbox_height = max_y - min_y

        # Notes on data format:
        # order: (frame index, person id, bbox left, bbox top, bbox width,
        # bbox_height, confidence, x, y, z)
        # Frame indices are 1-indexed in MOT format.
        # Confidence value is only used as a binary flag for evaluation.
        # x,y,z values are ignored for 2D detection (set to -1).
        results.append((
            f"{frame_idx+1:n}, {id:n}, {min_x:.2f}, {min_y:.2f}, "
            f"{bbox_width:.2f}, {bbox_height:.2f}, 1, -1, -1, -1"
        ))


def main():
    start = datetime.datetime.now()

    # Parse command line args.
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-b", "--backend", type=str, default="openpose_wrapper",
        help=(f"Deep Learning Backend used for pose estimation and tracking."
              f"Supported Backends: {AVAILABLE_BACKENDS}")
    )

    # SORT Object Tracker configuration arguments.
    parser.add_argument(
        "--max-age", type=int, default=8,
        help="Max Age parameter used for configuring the SORT Object Tracker."
    )
    parser.add_argument(
        "--min-hits", type=int, default=1,
        help="Min Hits parameter used for configuring the SORT Object Tracker."
    )
    parser.add_argument(
        "--iou-threshold", type=float, default=0.1,
        help="IoU Threshold parameter used for configuring the SORT Object Tracker."
    )

    # Parse Args and Configure Params
    args = parser.parse_args()

    pose_backend_name = args.backend
    if pose_backend_name == "openpose_wrapper":
        name = "openpose"
        pose_backend_params = {"enable_opencv_dnn": False}
    elif pose_backend_name == "openpose_opencv":
        name = "openpose"
        pose_backend_params = {"enable_opencv_dnn": True}
    else:
        logger.error((
            f"Deep Learning Backend '{pose_backend_name}' is not supported for "
            f"Pose Tracking. Please select one of the supported backends: "
            f"{AVAILABLE_BACKENDS}"
        ))
        sys.exit(1)

    object_tracker_params = {
        "max_age": args.max_age,
        "min_hits": args.min_hits,
        "iou_threshold": args.iou_threshold
    }

    # Create output directory if it does not exist.
    output_path = os.path.join(OUTPUT_BASE_DIR, pose_backend_name, OUTPUT_DATA_DIR)
    Path(output_path).mkdir(parents=True, exist_ok=True)

    # Initiate LivePose Deep Learning Backend.
    pose_backend: Optional[PoseBackend] = PoseBackend.create_from_name(
        name=name,
        object_tracker_params=object_tracker_params,
        **pose_backend_params
    )

    if pose_backend is None:
        logger.error((
            f"Could not load Deep Learning Model '{name}'. "
            f"Model name must be one of "
            f"{[key for key in PoseBackend.registered_backends.keys()]}"
        ))
        sys.exit(1)

    if not pose_backend.start():
        logger.error("Unable to start Deep Learning Backend.")
        sys.exit(1)
    
    # Process each MOT Challenge video separately.
    for seq_path in MOT_20_SEQUENCE_PATHS:
        seq_start = datetime.datetime.now()
        # Final results for this sequence in MOT format.
        seq_results = []

        seq_id = int(seq_path[-1])

        # Get directory containing frames for this sequence.
        image_directory = os.path.join(
            MOT_20_TRAIN_DIR,
            seq_path,
            IMAGE_DIR
        )

        # File paths for all images that make up the sequence.
        image_paths = glob.glob(os.path.join(image_directory, "*"))

        for frame_idx, image_path in enumerate(image_paths):
            # Run inference on single image frame.
            image = {"frame": cv2.imread(image_path)}

            res = pose_backend.step(images=[image], now=None, dt=None)

            if not res or pose_backend.tracked_image is None:
                logger.error((
                    f"Deep Learning Model failed to process image "
                    f"{image_path}"
                ))

            # Process results from bounding boxes.
            if pose_backend.bounding_boxes:
                bounding_boxes = pose_backend.bounding_boxes[0]

                add_to_results(bounding_boxes, frame_idx, seq_results)

            if False:
                cv2.imshow("LivePose MOT20 Challenge", pose_backend.tracked_image)
                cv2.waitKey(1)

        # Control test to see if order of results changes accuracy.
        # (it should not).
        if False:
            random.shuffle(seq_results)

        # Final results should be a line-separated string to write to file.
        seq_results = "\n".join(seq_results)

        # Write results to text file.
        output_file = os.path.join(output_path, seq_path) + ".txt"
        with open(output_file, "w") as fp:
            fp.write(seq_results)

        seq_end = datetime.datetime.now()
        print((
            f"{seq_path} sequence finished, "
            f"processed {len(image_paths)} frames in {seq_end - seq_start}"
        ))

    end = datetime.datetime.now()
    print(f"All sequences finished, total runtime was {end - start}")


if __name__ == "__main__":
    main()
