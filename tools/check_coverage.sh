#!/bin/bash
set -e

coverage run -m unittest discover -s livepose/tests -t .
# Save coverage report as .txt and .html files.
coverage report | tee coverage_report.txt
coverage html
